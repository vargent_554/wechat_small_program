var app = getApp();
Page({
    data:{
        myChildAccountData:[],
        hasData:true
    },
    toChildAccount:function(e){
        // +wx.getStorageSync('sessionid')
        let patentOpenId=wx.getStorageSync('sessionid');
        let childid=e.currentTarget.dataset.childid;
        let headimg=e.currentTarget.dataset.headimg;
        wx.navigateTo({
            url:'../childAccountDetail/childAccountDetail?type=childAccount&parentOpenId=' +
            ''+patentOpenId+'&childid='+childid+'&headimg='+headimg
        });
    },
    getMyChildAccount:function(){
        wx.showLoading({
            title: '加载中',
        });
        app.handlerPost('/api/account/doMySubAccount.do',{userToken:wx.getStorageSync('sessionid')},"GET").then( (res)=>{
            if(res.data.msg=="SUCCESS"){
                 let data=res.data.data.items;
                if(data.length>0){
                    this.setData({
                        myChildAccountData:data,
                        hasData:true
                    });
                }else{
                    this.setData({
                        hasData:false
                    });
                }

            }
            app.fetchAddressList();
        }).catch( (errMsg)=>{
            console.log(errMsg);
        } );
    },
    onLoad:function(){
        if(wx.getStorageSync('sessionid')==''){
            wx.login({
                success: function (r) {
                    app.login({code:r.code});
                }
            });
        }
      this.getMyChildAccount();
    }
});