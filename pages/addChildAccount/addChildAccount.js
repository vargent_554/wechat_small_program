var app = getApp();
Page({
    data:{
       addChildAccountCode:'',
        userInfo:{}
    },
    getAddChildAccountCode:function(){
        let getAddChildAparams={
            userToken:wx.getStorageSync('sessionid'),
            openId:wx.getStorageSync('sessionid')
        };
        wx.showLoading({
            title: '加载中',
        });
        app.handlerPost('/api/account/getSingleMiniImg.do',getAddChildAparams,"GET").then( (res)=>{
            if(res.data.code=="SUCCESS"){
                app.fetchAddressList();
               let codeUrl=res.data.data;
               this.setData({
                   addChildAccountCode:codeUrl
               });
            }
        }).catch( (errMsg)=>{
            console.log(errMsg);
        } );
    },
    previewImg: function (e) {
        let imgurl = e.currentTarget.dataset.imgurl;
        let uploadImgs=[];
        uploadImgs.push(imgurl);
        wx.previewImage({
            current: uploadImgs[0],
            //所有图片.
            urls: uploadImgs
        })
    },
    onShareAppMessage: function (res) {
        let parentOpenId=wx.getStorageSync('sessionid');
        if (res.from === 'button') {
            console.log(res.target)
        }
        return {
            title: '美璟小程序',
            path: '/pages/userRegister/userRegister?parentOpenId='+parentOpenId,
            success: function (res) {
                console.log(res);
            },
            fail: function (res) {
                console.log(res);
            }
        }
    },
    getPersonInfo:function(){
        app.handlerPost('/api/account/doUserInfo.do',{userToken:wx.getStorageSync('sessionid')}).then( (res)=>{
            if(res.data.data.memberType=='2'){
                this.getAddChildAccountCode();
            }else{
                wx.navigateTo({
                    url: '../userRegister/userRegister?parentOpenId='+options.parentOpenId,
                })
            }
        }).catch( (errMsg)=>{
            console.log(errMsg);
        } );
    },
    onLoad:function(options){
        if(wx.getStorageSync('sessionid')==''){
            wx.login({
                success: function (r) {
                    app.login({code:r.code});
                }
            });
        }else{
            this.getPersonInfo();
        }
    }
});