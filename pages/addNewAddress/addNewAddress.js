const AreaData = require("../../lib/Area.js");
const app = getApp();
Page({
    data: {
        defaultSelected:false,
        theTipShow:true,
        showDialog:false,
        theProCityValue:'省份、城市、区县',
        showCityTitle:"省份",
        setProCityDataC:false,
        buyerName: '',                                                 //收件人
        buyerPhone: '',                                                //收件人电话
        detailAddress: '',                                              //详情地址
        addressName: '',                                                //所在地区
        isdefault: false,                                               //控制设置默认地址
        provId: '',                                                     //省ID
        cityId: '',                                                     //市ID
        areaId: '',                                                     //区ID
        provName:'',
        cityName:'',
        areaName:'',
        value: [0, 0, 0],
        tempValue: [0, 0, 0],
        provArr: AreaData.result,                                       //省数组
        cityArr: AreaData.result[0].city,                               //市数组
        areaArr: AreaData.result[0].city[0].area,                       //区数组
        type: '',
        addressId:0,
        flag:true,
        typeAddress:'',
        detailTip:true,
        btnStatus:false
    },
    choiceProCityData:function(e){
        this.setData({
            showDialog:!this.data.showDialog
        });
    },
    choiceCitySure: function (e) {
        this.setData({
            setProCityDataC:true
        });
        if(this.data.buyerName!=''&&this.data.buyerPhone!=''&&this.data.detailAddress!=''&&this.data.setProCityDataC==true){
            this.setData({
                btnStatus:true
            });
        }else{
            this.setData({
                btnStatus:false
            });
        }
        let val = this.data.value;
        let provName = AreaData.result[val[0]].name;
        let cityName = AreaData.result[val[0]].city[val[1]].name;
        let areaName = AreaData.result[val[0]].city[val[1]].area[val[2]].name;
        let addressName = provName + cityName + areaName;
        let provId = AreaData.result[val[0]].id;
        let cityId = AreaData.result[val[0]].city[val[1]].id;
        let areaId = AreaData.result[val[0]].city[val[1]].area[val[2]].id;
        this.setData({
            addressName: addressName,
            provName:provName,
            cityName:cityName,
            areaName:areaName,
            provId: provId,
            cityId: cityId,
            areaId: areaId,
            showDialog:!this.data.showDialog,
            theProCityValue:  addressName
        })
    },
    formSubmit: function(e) {
        var warn = "";
        var flag = true;
        if(e.detail.value.nameValue==""){
            warn = "请填写您的姓名！";
        }else if(e.detail.value.phoneNum==""){
            warn = "请填写您的手机号！";
        }else if(!(/^1(3|4|5|7|8)\d{9}$/.test(e.detail.value.phoneNum))){
            warn = "手机号格式不正确";
        }else if(e.detail.value.addressInfo=='省份、城市、区县'){
            warn = "请选择您的地址"
        }else if(e.detail.value.detailAddress==""){
            warn = "请输入您的详细地址";
        }else{
            flag=false;
            this.setData({
                flag:flag
            });
            let addressparams={
                userToken:wx.getStorageSync('sessionid'),
                userName:this.data.buyerName,
                userPhone:this.data.buyerPhone,
                area1: this.data.provName,
                area2: this.data.cityName,
                area3: this.data.areaName,
                area4:'',
                code1: this.data.provId,
                code2:this.data.cityId,
                code3: this.data.areaId,
                code4:'',
                address:this.data.detailAddress,
                postCode:'',
                defaultAddress: this.data.isdefault?1:0
            },url;
            if(this.data.addressId){
                url='doUpdateMyUserAddress.do';
                addressparams.id=this.data.addressId;
            }else{
                url='doAddMyUserAddress.do'
            }
            app.handlerPost('/api/account/'+url,addressparams,"GET").then( (res)=>{
                if(res.data.msg=="SUCCESS"){
                    wx.showToast({
                        title: '添加地址成功',
                        icon: 'success',
                        duration: 3000
                    });
                    wx.navigateTo({
                        url: '../addressManage/addressManage?typeAddress='+this.data.typeAddress,
                    })
                }
            }).catch( (errMsg)=>{
                console.log(errMsg);
            } );
        };
        if(flag){
            wx.showModal({
                title: '提示',
                content: warn
            });
        }
    },
    selectDefultAdd:function(){
        this.setData({
            isdefault:!this.data.isdefault
        })
    },
    value:function(){
        this.setData({
            theTipShow:false
        });
    },
    onLoad:function(options){
        if(wx.getStorageSync('sessionid')==''){
            wx.login({
                success: function (r) {
                    app.login({code:r.code});
                }
            });
        }
        wx.setNavigationBarTitle({
            title: options.navbartitle,
        });
        let that = this;
        that.setData({
            type: options.type,
            typeAddress:options.typeAddress
        });
        if (options.type == 1) {
            let addressData = JSON.parse(options.addressData);
             that.saveAddressData(addressData);
             that.setData({
                 addressId:options.id,
                 btnStatus:true
             });
        }
    },
    saveAddressData(addressData) {
        let ProvArr = AreaData.result;
        let valArr = [];
        // 遍历省数组
        for (let i = 0; i < ProvArr.length; i++) {
            // 找到省对应的id
            if (ProvArr[i].id == addressData.province) {
                //提取对应省名
                let provName = ProvArr[i].name;
                // 提取对应省名在数组中对应的id
                valArr.push(i);
                // 提取对应省名下的城市数组
                let cityArr = ProvArr[i].city;
                // 遍历对应省名下的城市数组
                for (let j = 0; j < ProvArr[i].city.length; j++) {
                    // 找到市对应的id
                    if (ProvArr[i].city[j].id == addressData.city) {
                        // 提取对应市名
                        let cityName = ProvArr[i].city[j].name;
                        // 提取对应市名在数组中对应的id
                        valArr.push(j);
                        // 提取对应市名下的区数组
                        let areaArr = ProvArr[i].city[j].area;
                        // 遍历对应市名下的区数组
                        for (let k = 0; k < ProvArr[i].city[j].area.length; k++) {
                            // 找到区对应的id
                            if (ProvArr[i].city[j].area[k].id == addressData.region) {
                                // 提取对应区名
                                let areaName = ProvArr[i].city[j].area[k].name;
                                // 提取对应区名在数组中对应的id
                                valArr.push(k);
                                let addressName = provName + cityName + areaName;
                                this.setData({
                                    buyerName: addressData.name,
                                    buyerPhone: addressData.buyerPhone,
                                    detailAddress: addressData.address,
                                    theProCityValue: addressName,
                                    value: valArr,
                                    cityArr: cityArr,
                                    areaArr: areaArr,
                                    provName:provName,
                                    cityName:cityName,
                                    areaName:areaName,
                                    provId: addressData.province,
                                    cityId: addressData.city,
                                    areaId: addressData.region,
                                    setProCityDataC:true,
                                    isdefault: addressData.isdefault == 1 ? true : false
                                })
                            }
                        }
                    }
                }

            }
        }
    },
    //收件人
    bindNameInput(e) {
        this.setData({ buyerName: e.detail.value });
        if(this.data.buyerName!=''&&this.data.buyerPhone!=''&&this.data.detailAddress!=''&&this.data.setProCityDataC==true){
            this.setData({
                btnStatus:true
            });
        }else{
            this.setData({
                btnStatus:false
            });
        }
    },
    bindPhoneNumInput(e) {
        this.setData({ buyerPhone: e.detail.value });
        if(this.data.buyerName!=''&&this.data.buyerPhone!=''&&this.data.detailAddress!=''&&this.data.setProCityDataC==true){
            this.setData({
                btnStatus:true
            });
        }else{
            this.setData({
                btnStatus:false
            });
        }
    },
    changeDetailAddressChange:function(e){
        this.setData({
            detailTip:false
        });
    },
    bindDetailAddress(e) {
        if(e.detail.value==''){
            this.setData({
                detailTip:true
            });
        }
        this.setData({
            detailAddress: e.detail.value
        });
        if(this.data.buyerName!=''&&this.data.buyerPhone!=''&&this.data.detailAddress!=''&&this.data.setProCityDataC==true){
            this.setData({
                btnStatus:true
            });
        }else{
            this.setData({
                btnStatus:false
            });
        }
    },
    bindChange: function (e) {
        let val = e.detail.value;
        if (val[0] != this.data.tempValue[0]) {
            val = [val[0], 0, 0]
        }
        if (val[1] != this.data.tempValue[1]) {
            val = [val[0], val[1], 0]
        }
        this.setData({
            tempValue: val,
            value: val,
            cityArr: AreaData.result[val[0]].city,
            areaArr: AreaData.result[val[0]].city[val[1]].area,
        })
    },

});