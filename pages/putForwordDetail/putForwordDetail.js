//获取应用实例
const app = getApp();
app.getCommonTime();
app.getTrim();
Page({
    data: {
        getForwordData:[],
        creatTime:'',
        hasData:true
    },
    getDetailForwordRequest:function(){
        let accountId=wx.getStorageSync('accountId');
        app.handlerPost('/api/account/doAccountOutOrderPage.do',{userToken:wx.getStorageSync('sessionid'),accountId:accountId}).then( (res)=>{
            if(res.data.code=='SUCCESS'){
                let data=res.data.data.items;
                if(data.length>0){
                    let creatTime=[];
                    for(var i=0;i<data.length;i++){
                        creatTime.push(new Date(data[i].createTime).Format("yyyy-MM-dd hh-mm-ss"))
                    }
                    this.setData({
                        getForwordData:data,
                        creatTime:creatTime,
                        hasData:true
                    });
                }else{
                    this.setData({
                        hasData:false
                    });
                }

            }
        }).catch( (errMsg)=>{
            console.log(errMsg);
        } );
    },
    getCommisionForWord:function(){
        let accountId=wx.getStorageSync('accountId');
        app.handlerPost('/api/account/doSubAccountYjPage.do',{data:'2018-07-01',accountId:accountId}).then( (res)=>{
            if(res.data.code=='SUCCESS'){
                let data=res.data.data.items;
                if(data.length>0){
                    let creatTime=[];
                    for(var i=0;i<data.length;i++){
                        creatTime.push(new Date(data[i].createTime).Format("yyyy-MM-dd hh-mm-ss"))
                    }
                    this.setData({
                        getForwordData:data,
                        creatTime:creatTime,
                        hasData:true
                    });
                }else{
                    this.setData({
                        hasData:false
                    });
                }

            }
        }).catch( (errMsg)=>{
            console.log(errMsg);
        } );
    },
    onLoad:function(options){
        if(wx.getStorageSync('sessionid')==''){
            wx.login({
                success: function (r) {
                    app.login({code:r.code});
                }
            });
        }
        if(options.type=='detailForword'){
            this.getDetailForwordRequest();
        }else if(options.type=='commisionFor'){
            this.getCommisionForWord();
        }

    }
});