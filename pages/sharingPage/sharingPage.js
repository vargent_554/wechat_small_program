var app = getApp();
Page({
  data: {
    itemCode: '',
    prostImgSrc: '',
  },

  onShareAppMessage(res) {
    return {
      title: '深度体验全球好房',
      path: '/pages/vrPageExhibt/vrPageExhibt?type=share&parentOpenId=' +
        wx.getStorageSync('sessionid') +
        '&outUrl=https://mzgf.com/vr-1.0/' +
        this.data.itemCode,
      imageUrl: 'https://mzgf.oss-cn-hangzhou.aliyuncs.com/' + this.data.prostImgSrc,
      success: function (res) {
        console.log(res);
      },
      fail: function (res) {
        console.log(res);
      }
    };
  },
  share() {
    wx.showShareMenu();
  },

  goback() {
    wx.navigateBack({
      changed: true
    });
  },

  change(itemCode) {
    itemCode = typeof itemCode == "string" ? itemCode : itemCode.toString();
    if (itemCode.length == 1) {
      itemCode = '00' + itemCode
    } else if (itemCode.length == 2) {
      itemCode = '0' + itemCode
    } else {
      return itemCode

    }

    return itemCode
  },
  clg(){
    console.log(1)
  },

  onLoad(options) {

    app
      .handlerPost(
        "/api/plateForm/fenXimgById.do", {
          'id': options.itemId
        },
        'GET'
      )
      .then(res => {
        if (res.data.msg == 'SUCCESS') {
          console.log(res.data.data)

          this.setData({
            itemCode: this.change(res.data.data.proCode),
            prostImgSrc: res.data.data.posterImg
          })
        }
      })
      .catch(errMsg => {
        console.log(errMsg);
      });

  },
});