var app = getApp();
Page({
    data:{
        navData:[],
        totalClassifyData:[],
        swiperItemData:[],
        productListData:[],
        winHeight:"",//窗口高度
        currentTab:0, //预设当前项的值
        scrollLeft:0 ,//tab标题的滚动条位置
        commonUrl:app.globalData.commonUrl,
        scrollPageNum:1,
        orderTotalPage:'',
        onLoading:false,
        noMoreData:false,
        bindDownLoad:false,
        scrollTop:0,
    },
    // 滚动切换标签样式
    switchTab:function(e){
        this.setData({
            currentTab:e.detail.current,
            scrollPageNum:1,
            bindDownLoad:false
        });
        this.checkCor();
        this.getCategoryCodeData(this.data.navData[e.detail.current].code);
    },
    bindDownLoad:function(e){
        let that=this;
        that.setData({
            bindDownLoad:true
        });
        if(that.data.orderTotalPage==this.data.scrollPageNum){
            that.setData({
                noMoreData:true,
                onLoading:false,
            });
            return;
        }else{
            this.data.scrollPageNum++;
            that.setData({
                onLoading:true,
                scrollPageNum:this.data.scrollPageNum
            });
        }
        that.getCategoryCodeData(this.data.navData[this.data.currentTab].code);

    },
    refresh:function(e){
        wx.showNavigationBarLoading();
        let that=this;
        that.setData({
            scrollPageNum:1,
            bindDownLoad:false
        });
        that.getCategoryCodeData(this.data.navData[this.data.currentTab].code);
    },
    getCategoryCodeData:function(data){
        // wx.showLoading({
        //     title: '加载中',
        // });
        app.handlerPost('/api/product/doGetPdtByCCode.do',{'page.page':this.data.scrollPageNum,userToken:'',categorycode:data},"GET").then( (res)=>{
            if(res.data.msg=="SUCCESS"){
                let productListData=res.data.data.items;
                this.setData({
                    orderTotalPage:res.data.data.totalPage
                });
                if(this.data.scrollPageNum==1){
                    this.setData({
                        productListData:productListData
                    });
                }else{
                    this.setData({
                        productListData:this.data.productListData.concat(productListData),
                    });
                }

            }
            setTimeout(function() {
                // wx.hideLoading();
                wx.hideNavigationBarLoading();
                wx.stopPullDownRefresh();
            },500);
        }).catch( (errMsg)=>{
            console.log(errMsg);
        } );
    },
    // 点击标题切换当前页时改变样式
    swichNav:function(e){
        var cur=e.target.dataset.current;
       this.getCategoryCodeData(this.data.navData[cur].code);
        if(this.data.currentTaB==cur){return false;}
        else{
            this.setData({
                currentTab:cur
            });
        }
        this.checkCor();
    },
    //判断当前滚动超过一屏时，设置tab标题滚动条。
    checkCor:function(){
        this.setData({
            scrollLeft:45*this.data.currentTab
        })
    },
    onShareAppMessage: function (res) {
        if (res.from === 'button') {
            console.log(res.target)
        }
        return {
            title: '美璟小程序',
            path: '/pages/muZhiGongFangList/muZhiGongFangList?parentOpenId='+wx.getStorageSync('sessionid')+'&type=share'+'&code='+ wx.getStorageSync('code')+'&name='+wx.getStorageSync('name')+'&index='+wx.getStorageSync('index')+'&productdata='+wx.getStorageSync('productdata'),
            success: function (res) {
                console.log(res);
            },
            fail: function (res) {
                console.log(res);
            }
        }
    },
    onPullDownRefresh: function () {
        // 小程序提供的api，通知页面停止下拉刷新效果
        setTimeout(function () {
            wx.stopPullDownRefresh();
        }, 1000)
    },
    onLoad: function(options) {
        wx.login({
            success: function (r) {
                if(options.type=='share'){
                    wx.setStorageSync('parentOpenId', options.parentOpenId);
                    app.login({code:r.code,parentOpenId:options.parentOpenId});
                }else{
                    app.login({code:r.code});
                }
            }
        });
        let navArr=JSON.parse(options.productdata).pdtCategoryDtos,navData=[];
        for(var i=0;i<navArr.length;i++){
            navData.push({
                name:navArr[i].name,
                pid:navArr[i].pid,
                code:navArr[i].code
            });
        }
        wx.setNavigationBarTitle({
            title: options.name
        });
        this.setData({
            currentTab:options.index,
            scrollLeft:options.index*45,
            navData:navData,
            totalClassifyData:navArr
        });
        wx.setStorageSync('productdata', options.productdata);
        wx.setStorageSync('index',options.index);
        wx.setStorageSync('name', options.name);
        wx.setStorageSync('code', options.code);
        this.getCategoryCodeData(options.code);
        var that = this;
        for(var i=0;i<this.data.navData.length;i++){
            this.data.swiperItemData.push(i);
        }
        this.setData({
            swiperItemData:this.data.swiperItemData
        });
        //  高度自适应
        wx.getSystemInfo( {
            success: function( res ) {
                var clientHeight=res.windowHeight,
                    clientWidth=res.windowWidth,
                    rpxR=750/clientWidth;
                var  calc=clientHeight*rpxR;
                that.setData( {
                    winHeight: calc
                });
            }
        });
    },
    footerTap:app.footerTap,
    goGoodDetail:function(e){
        let id=e.currentTarget.dataset.id;
        wx.navigateTo({
            url: '../goodDetail/goodDetail?id='+id
        })
    }
});