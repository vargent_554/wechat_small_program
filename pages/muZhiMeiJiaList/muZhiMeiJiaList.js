var app = getApp();
Page({
    data:{
        navData:["全部","沙发","座具","几类","桌类","床类","柜类","架类","配饰"],
        swiperItemData:[],
        winHeight:"",//窗口高度
        currentTab:0, //预设当前项的值
        scrollLeft:0 //tab标题的滚动条位置
    },
    onPullDownRefresh: function () {
        // 小程序提供的api，通知页面停止下拉刷新效果
        setTimeout(function () {
            wx.stopPullDownRefresh();
        }, 1000)
    },
    // 滚动切换标签样式
    switchTab:function(e){
        this.setData({
            currentTab:e.detail.current
        });
        this.checkCor();
    },
    onShareAppMessage: function (res) {
        if (res.from === 'button') {
            console.log(res.target)
        }
        return {
            title: '美璟小程序',
            path: '/pages/muZhiMeiJiaList/muZhiMeiJiaList?userOpen='+wx.getStorageSync('sessionid'),
            success: function (res) {
                console.log(res);
            },
            fail: function (res) {
                console.log(res);
            }
        }
    },
    // 点击标题切换当前页时改变样式
    swichNav:function(e){
        var cur=e.target.dataset.current;
        if(this.data.currentTab==cur){return false;}
        else{
            this.setData({
                currentTab:cur
            })
        }
    },
    //判断当前滚动超过一屏时，设置tab标题滚动条。
    checkCor:function(){
        if (this.data.currentTab>4){
            this.setData({
                scrollLeft:300
            })
        }else{
            this.setData({
                scrollLeft:0
            })
        }
    },
    onLoad: function() {
        var that = this;
        for(var i=0;i<this.data.navData.length;i++){
            this.data.swiperItemData.push(i);
        }
        this.setData({
            swiperItemData:this.data.swiperItemData
        });
        //  高度自适应
        wx.getSystemInfo( {
            success: function( res ) {
                var clientHeight=res.windowHeight,
                    clientWidth=res.windowWidth,
                    rpxR=750/clientWidth;
                var  calc=clientHeight*rpxR-180;
                that.setData( {
                    winHeight: calc
                });
            }
        });
    },
    footerTap:app.footerTap
});