let app = getApp();
Page({
    data: {
        itemCode: '001',
        proId: '',
        messageList: [],
        message: '',
        focus: false,
        huifuid: -1,
        userId: '',
        userNo: '',
        page: 1,
        qiehuantext: "时间",
        dropdown: false,
        redden: 0,
        searchtype: ['时间', '热度'],
        lastTime: '',
    },

    switchshow(dropdown = !this.data.dropdown) {
        console.log(1)
        this.setData({
            dropdown
        })
    },

    switch (e) {

        let index = e.currentTarget.dataset.index;
        this.setData({
            qiehuantext: this.data.searchtype[index],
            redden: index,
            dropdown: !this.data.dropdown,
            lastTime: new Date().getTime(),
            messageList: [],
            page :  e.currentTarget.dataset.index == 1 ? this.data.page : 1
        })
        setTimeout(
            () => this.getmessageList(), 200)

    },

    huifu(e) {
        console.log(e.currentTarget.dataset.id);
        this.setData({
            focus: true,
            huifuid: e.currentTarget.dataset.id,
        });
    },

    msginput(e) {
        this.setData({
            message: e.detail.value,
        });
    },

    send(e) {

        let list = this.data.messageList;
        let url = '/api/plateForm/subUserComments.do';

        // app.login({code : e.detail.userInfo});
        wx.showLoading({
            title: '加载中',
        });

        const item = {
            userName: e.detail.userInfo.nickName,
            headImg: e.detail.userInfo.avatarUrl,
            contents: this.data.message,
            userId: this.data.userId
        }

        const data = {
            proId: this.data.proId,
            userToken: wx.getStorageSync('sessionid'),
            contents: this.data.message,
            userId: this.data.userId
        };

        if (this.data.huifuid > -1) {

            data.id = list[this.data.huifuid].id;
            item.userCommentsDto = list[this.data.huifuid];
            url = '/api/plateForm/replyCommits.do'
        }

        app
            .handlerPost(
                url,
                data,
                'POST'
            )
            .then(res => {
                if (res.data.msg == 'SUCCESS') {

                    wx.hideLoading();

                    item.createTime = new Date(new Date().getTime()).Format("yyyy-MM-dd hh:mm:ss");

                    const list = this.data.messageList;

                    list.unshift(item);

                    this.setData({
                        huifuid: -1,
                        messageList: list,
                        focus: false,
                        message: ''
                    })

                }
            })
            .catch(errMsg => {
                console.log(errMsg);
            });

    },

    dianzan(e) {
        wx.showLoading({
            title: '加载中',
        });
        const item = this.data.messageList[e.currentTarget.dataset.id];


        console.log(typeof item.pointPraise)
        item.pointPraise = item.pointPraise > 0 ? item.status ? item.pointPraise - 1 : item.pointPraise + 1 : 0;
        item.status = item.status ? 0 : 1;
        const data = {
            proId: this.data.proId,
            commentId: item.id,
            userToken: wx.getStorageSync('sessionid'),
            userId: this.data.userId,
            status: item.status
        }
        app
            .handlerPost(
                '/api/plateForm/pointProject.do',
                data,
                'POST'
            )
            .then(res => {
                if (res.data.msg == 'SUCCESS') {

                    wx.hideLoading();

                    const list = this.data.messageList;

                    list[e.currentTarget.dataset.id] = item;

                    this.setData({
                        messageList: list
                    })

                }
            })
            .catch(errMsg => {
                console.log(errMsg);
            });
    },

    filter(Attributes, value, list) {
        for (const key in list) {
            if (list.hasOwnProperty(key)) {
                if (list[key][Attributes] == value) {
                    return list[key];
                }
            }
        }
    },

    datechange(data, createTime = "createTime") {
        data.forEach(item => {
            item[createTime] ? item[createTime] = new Date(item[createTime]).Format("yyyy-MM-dd hh:mm:ss") : item[createTime];
            item["pointPraise"] ? item["pointPraise"] = parseInt(item["pointPraise"]) : 0;
            item["status"] ? item["status"] = parseInt(item["status"]) : item["status"];
        });
    },

    getmessageList(proId = this.data.proId, userNo = this.data.userNo) {

        
        let time = this.data.lastTime ? this.data.lastTime : new Date().getTime();
        let url = this.data.redden == 0 ? "/api/plateForm/userCommentsPage.do" : '/api/plateForm/pagePointDesc.do';
        console.log( this.data.page);
        app
            .handlerPost(
                url, {
                    proId,
                    userId: userNo,
                    'createTime': time,
                    'page.page': this.data.page,
                },
                'GET'
            )
            .then(res => {
                if (res.data.msg == 'SUCCESS') {
                    let time = res.data.data[res.data.data.length - 1].createTime;
                    this.datechange(res.data.data);
                    let page = this.data.redden == 0 ? this.data.page : this.data.page + 1;
                    this.setData({
                        messageList: this.data.messageList.concat(res.data.data),
                        lastTime: time,
                        page
                    })
                }
            })
            .catch(errMsg => {
                console.log(errMsg);
            });
    },

    onReachBottom() {
        this.getmessageList()
    },

    onLoad(options) {
        
        if (wx.getStorageSync('sessionid') == '') {
            wx.login({
                success: function (r) {
                    app.login({
                        code: r.code
                    });
                }
            });
        }

        options.proId =  options.proId ? options.proId : 18072100001013184;

        var openid = wx.getStorageSync('sessionid');

        let that = this;
        wx.request({
            url: 'https://api.mzgf.com/api/account/doUserInfo.do',
            data: {
                userToken: openid
            },
            method: "GET",
            header: {
                'content-type': "application/json",
            },
            success: function (res) {

                console.log(res.data.data)
                that.setData({
                    userId: res.data.data.id,
                    userNo: res.data.data.userNo,
                    proId: options.proId
                })
                that.getmessageList(options.proId, res.data.data.userNo)
            }
        })

    }


});