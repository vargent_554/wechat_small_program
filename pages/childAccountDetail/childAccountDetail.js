var app = getApp();
app.getCommonTime();
Page({
    data:{
        showFilter:false,
        getDoSubAccountDetalData:[],
        getDoSubAccountYjPageData:[],
        createTime:[],
        childid:'',
        headimg:'../../images/person.png'
    },
    settingBtn:function(e) {
        let settingData=e.detail.value;
        this.doSetSubAccountYj(settingData);

    },
    doSetSubAccountYj:function(value){
        let params={
            userToken:wx.getStorageSync('sessionid'),
            accountId:this.data.childid,
            disAccount:value
        };
        wx.showLoading({
            title: '加载中',
        });
        app.handlerPost('/api/account/doSetSubAccountYj.do',params,"GET").then( (res)=>{
            if(res.data.code=="SUCCESS"){
                let data=res.data.data;
                wx.showToast({
                    title: '设置佣金比例成功',
                    icon: 'success',
                    duration: 2000
                })
                this.getDoSubAccountDetal();
            }
            app.fetchAddressList();
        }).catch( (errMsg)=>{
            console.log(errMsg);
        } );
    },
    showFilterTime:function(){
        this.setData({
            showFilter:!this.data.showFilter
        });
    },
    getDoSubAccountDetal:function(id){
        let doSubAccountDetalParams={
            userToken:wx.getStorageSync('sessionid'),
            accountId:id
        }
        wx.showLoading({
            title: '加载中',
        });
        app.handlerPost('/api/account/doSubAccountDetal.do',doSubAccountDetalParams,"GET").then( (res)=>{
            if(res.data.msg=="SUCCESS"){
                let data=res.data.data;
                this.setData({
                    getDoSubAccountDetalData:data
                });
            }
            app.fetchAddressList();
        }).catch( (errMsg)=>{
            console.log(errMsg);
        } );
    },
    onPullDownRefresh: function () {
        // 小程序提供的api，通知页面停止下拉刷新效果
        let that=this;
        setTimeout(function() {
            that.getDoSubAccountDetal();
            that.getDoSubAccountYjPaget();
            wx.hideNavigationBarLoading();
            wx.stopPullDownRefresh();
        },1000);

    },
    getDoSubAccountYjPaget:function(id,data){
        let params={
            accountId:id,
            data:data
        };
        wx.showLoading({
            title: '加载中',
        });
        app.handlerPost('/api/account/doSubAccountYjPage.do',params,"GET").then( (res)=>{
            if(res.data.msg=="SUCCESS"){
                let data=res.data.data.items,createTime=[];
                for(var i=0;i<data.length;i++){
                    createTime.push(new Date(data[i].createTime).Format("yyyy-MM-dd hh-mm-ss"));
                }
                this.setData({
                    getDoSubAccountYjPageData:data,
                    createTime:createTime
                });
            }
            app.fetchAddressList();
        }).catch( (errMsg)=>{
            console.log(errMsg);
        } );
    },
    onLoad:function(options){
        if(wx.getStorageSync('sessionid')==''){
            wx.login({
                success: function (r) {
                    app.login({code:r.code});
                }
            });
        }
        if(options.childid){
            this.setData({
                childid:options.childid,
                headimg:options.headimg
            });
        }
        let date = new Date(), today = date.Format("yyyy-MM-dd");
        this.getDoSubAccountDetal(options.childid);
        this.getDoSubAccountYjPaget(options.childid,today);
    }
});