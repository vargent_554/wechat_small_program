var app = getApp();
Page({
    data:{
        canIUse: wx.canIUse('button.open-type.getUserInfo'),
    },
    bindGetUserInfo: function (e) {
        var that=this;
        that.getUserInfo('',e);
    },
    getUserInfo: function(cb,res) {
        wx.login({
            success: function(r) {
                app.handlerPost('/api/account/doRegisterUser.do', {
                    code: r.code,
                    parentOpenId: wx.getStorageSync('parentOpenId'),
                    nickname: res.detail.userInfo.nickName,
                    headimgurl:res.detail.userInfo.avatarUrl
                }).then( (res)=>{
                    wx.showToast({
                        title: '登录成功',
                        icon: 'success',
                        duration: 2000,
                    });
                    wx.switchTab({
                        url:'../index/index',
                        success:function(){
                        },
                    });
                }).catch( (errMsg)=>{
                    console.log(errMsg);
                } );
                wx.setStorageSync('userinfo', res.detail.userInfo);
            }
        })
    },
    onLoad: function() {

    },

});