const app = getApp();
Page({
    data: {
        curNav: 1,
        curIndex: 0,
        scrollTop: 0,
        cateItemCategory:[],
        commonUrl:app.globalData.commonUrl
},
    onPullDownRefresh: function () {
        // 小程序提供的api，通知页面停止下拉刷新效果
        wx.stopPullDownRefresh;
    },
    fetchAddressList(callback) {
        setTimeout(res => {
            if(callback){
                callback();
            }
        }, 500);

    },
    getProductList:function(){
        app.handlerPost('/api/product/doGetCategory.do',{userToken:''},"GET").then( (res)=>{
            if(res.data.msg=="SUCCESS"){
                setTimeout(res => {
                    wx.hideLoading();
                }, 500);
                this.setData({
                    cateItemCategory:res.data.data
                });
            }
        }).catch( (errMsg)=>{
            console.log(errMsg);
        } );
    },
    onLoad:function(options){
        wx.login({
            success: function (r) {
                if(options.type=='share'){
                    wx.setStorageSync('parentOpenId', options.parentOpenId);
                    app.login({code:r.code,parentOpenId:options.parentOpenId});
                }else{
                    app.login({code:r.code});
                }
            }
        });
        /**
         * 获取产品分类
         * **/
        this.getProductList();
        wx.login({
            success: function (r) {
                if(options.type=='share'){
                    app.login({code:r.code,parentOpenId:options.parentOpenId});
                }else{
                    app.login({code:r.code});
                }
            }
        });
    },
    //事件处理函数
    switchRightTab: function (e) {
        let id = e.target.dataset.id,
            index = parseInt(e.target.dataset.index);
        wx.showLoading({
            title: '加载中',
        });
        this.getProductList();
        this.setData({
            curNav: id,
            curIndex: index
        });
    },
    onShareAppMessage: function (res) {
        if (res.from === 'button') {
            console.log(res.target)
        }
        return {
            title: '美璟小程序',
            path: '/pages/BeauSceneryShopCart/BeauSceneryShopCart?type=share&parentOpenId='+wx.getStorageSync('sessionid'),
            success: function (res) {
                console.log(res);
            },
            fail: function (res) {
                console.log(res);
            }
        }
    },
    toSearchPage:function(){
        wx.navigateTo({
            url: '../searchPage/searchPage?type=1'
        })
    },
    tomuZhiGongFangList:function(e){
        let code=e.currentTarget.dataset.code;
        let name=e.currentTarget.dataset.name;
        let index=e.currentTarget.dataset.index;
        let productdata=JSON.stringify(e.currentTarget.dataset.productdata);
        wx.navigateTo({
            url: '../muZhiGongFangList/muZhiGongFangList?type=jump'+'&code='+code+'&name='+name+'&index='+index+'&productdata='+productdata
        })
    },
});