//获取应用实例
const app = getApp();
Page({
  data: {
      searchText:'',
      autoFocus:true,
      hotSearchListTip:["四平面","云椅","涟漪斗柜","徐小姐的家"],
      currentTab:-1,
      searchPageResultData:[],
      searchType:'',
      searchProjectResultData:[],
      showEtherStatus:false,
      hideHotTip:true,
      hasSearchData:true,
      searchPage:true,
      showCancelBtn:false,
      commonUrl:app.globalData.commonUrl,
      showCancel:false,
  },
    goGoodDetail:function(e){
        let id=e.currentTarget.dataset.id;
        wx.navigateTo({
            url: '../goodDetail/goodDetail?id='+id
        })
    },
    changeInValue:function(e){
      if(e.detail.value!=''){
          this.setData({
              showCancelBtn:true,
              searchText:e.detail.value
          });
      }else{
          this.setData({
              showCancelBtn:false
          });
      }

    },
    goOutUrl:function(e){
        let outUrl=e.currentTarget.dataset.outurl;
        wx.navigateTo({
            url: '../vrPageExhibt/vrPageExhibt?outUrl='+outUrl
        })

    },
    getVideoModal:function(e){
        if(e.currentTarget.dataset.src){
            this.setData({
                videoSrc:e.currentTarget.dataset.src
            });
        }
        this.setData({
            showDialog:!this.data.showDialog
        });
        if(!this.data.showDialog){
            this.setData({
                controllsPlay:false
            });
        }else{
            this.setData({
                controllsPlay:true
            });
        }
    },
    choiceHotTip:function(e){
       var that=this;
       that.setData({
           searchText:that.data.hotSearchListTip[e.target.dataset.index],
           currentTab:e.target.dataset.index,
           autoFocus:true,
           searchPage:true,
           showCancelBtn:true,
       });
       this.getProjectResultData(that.data.hotSearchListTip[e.target.dataset.index]);

    },
    searchBtnClo:function(){
        this.getProjectResultData(this.data.searchText);
    },
    searchBtn:function(e) {
       let searchResult=e.detail.value;
        this.setData({
            searchPage:true,
        });
       this.getProjectResultData(searchResult);
    },
    cancelText:function(){
        this.setData({
            searchText:'',
            autoFocus:true,
            searchPage:true,
            showCancelBtn:false,
            hideHotTip:true,
            hasSearchData:true,
            searchProjectResultData:[]
        });
    },
    cancelSearch:function(){
        this.setData({
            searchText:'',
            hideHotTip:true,
            autoFocus:false,
            hasSearchData:true,
            searchPage:false,
            showCancelBtn:false,
            searchProjectResultData:[]
        });
        wx.navigateBack({
            changed: true
        })
    },
    getProjectResultData:function(name){
        wx.showLoading({
            title: '加载中',
        });
        app.handlerPost('/api/plateForm/plateFormPage.do',{userToken:wx.getStorageSync('sessionid'),proName:name},"GET").then( (res)=>{
            let data=res.data.data.items;
            if(data==''){
                this.setData({
                    hasSearchData:false,
                    hideHotTip:false
                });
            }
            this.setData({
                searchProjectResultData:data,
                hideHotTip:false
            });
            if(res.data.msg=="SUCCESS"){
                app.fetchAddressList();
            }
        }).catch( (errMsg)=>{
            console.log(errMsg);
        } );
    },
    onLoad: function () {
        if(wx.getStorageSync('sessionid')==''){
            wx.login({
                success: function (r) {
                    app.login({code:r.code});
                }
            });
        }
     }
});
