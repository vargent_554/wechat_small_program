//获取应用实例
const app = getApp();
app.getCommonTime();
Page({
    data: {
        newMassge: '个人中心',
        userInfo:{},
        commonUrl:app.globalData.commonUrl,
    },
    toMyCollectProject: function () {
        app.globalData.setValue=true;
        wx.switchTab({
            url:'../index/index',
            success:function(res){

            },
        })
    },
    getPersonInfo:function(){
        var openid=wx.getStorageSync('sessionid');
        app.handlerPost('/api/account/doUserInfo.do',{userToken:openid}).then( (res)=>{
            console.log(res)
            this.setData({
                userInfo:res.data.data,
                overDate:res.data.data.overDate!=''?new Date(res.data.data.overDate).Format("yyyy-MM-dd"):''
            });
            wx.setStorageSync('accountId',this.data.userInfo.id);
            wx.setStorageSync('myUserIInfo', res.data.data);
        }).catch( (errMsg)=>{
            console.log(errMsg);
        } );
    },
    onShow:function(){
        this.getPersonInfo();
    },
    callServer:function(){
        wx.makePhoneCall({
            phoneNumber: '110'
        })
    },
    onLoad: function () {
        if(wx.getStorageSync('sessionid')==''){
            wx.login({
                success: function (r) {
                    app.login({code:r.code});
                }
            });
        }
        this.getPersonInfo();
    },
  onTabItemTap: function (options) {
    console.log(options)
  }
});