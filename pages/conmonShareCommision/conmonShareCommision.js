var app = getApp();
Page({
    data:{
        choiceTab:0,
        comMemberData:['非会员','会员','代理账号','代理子账号'],
        commisionData:{},
        showStatusSubs:false,
        zhifuData:'',
    },
    zhifuNameInput:function(e) {
        let zhifuData = e.detail.value;
        this.setData({
            zhifuData: zhifuData
        })
    },
    subsUpdata1:function(){
        this.setData({
            showStatusSubs:true
        });
    },
    subsUpdata:function(e){
        let params={
            orderId:'',
            userToken:wx.getStorageSync('sessionid'),
            alipayAccount:''
        };
        let type=e.currentTarget.dataset.type;
        let that=this;
        if(this.data.commisionData.accountB==null||0){
            this.setData({
                showStatusSubs:true
            });
        }else{
            if(type=='tixian'){
                app.handlerPost('/api/account/doDrawCash.do',params,"GET").then( (res)=>{
                    let msg=res.data.msg;
                    if(msg=='用户未填写收款账户'){
                        this.setData({
                            showStatusSubs:true
                        });
                    }else{
                        wx.showModal({
                            icon:'error',
                            content:msg ,
                            success:function(res){
                                if(res.confirm){

                                }else if(res.cancel){

                                }
                            },
                        });
                    }
                    this.setData({
                        showStatusSubs:false
                    });
                }).catch( (errMsg)=>{
                    console.log(errMsg);
                } );
            }else if(type=='zhifuSave'){
                if(this.data.zhifuData!=''){
                    params.alipayAccount=this.data.zhifuData;
                    app.handlerPost('/api/account/doDrawCash.do',params,"GET").then( (res)=>{
                        if(res.data.code=='SUCCESS'){
                            wx.showToast({
                                title: '提现成功',
                                icon: 'success',
                                duration: 2000
                            })
                            this.getCommisionData();

                        }
                        this.setData({
                            showStatusSubs:false
                        });
                    }).catch( (errMsg)=>{
                        console.log(errMsg);
                    } );
                }else{
                    wx.showModal({
                        icon:'error',
                        content: '请输入支付宝账号',
                        success:function(res){
                            if(res.confirm){
                                return;
                            }else if(res.cancel){
                                that.setData({
                                    showStatusSubs:false
                                });
                            }
                        },
                    })
                }
            }else{
                this.setData({
                    showStatusSubs:false
                });
            }
        }
    },
    choiceTapC:function(e){
        this.setData({
            choiceTab:e.target.dataset.value
        });
        if(e.target.dataset.value==1){
            wx.navigateTo({
                url: '../childAccount/childAccount'
            })
        }
    },
    getCommisionData:function(){
        app.handlerPost('/api/account/doMyAccountInfo.do',{userToken:wx.getStorageSync('sessionid')}).then( (res)=>{
            if(res.data.code=='SUCCESS'){
                let commisionData=res.data.data;
                this.setData({
                    commisionData:commisionData
                });
            }
        }).catch( (errMsg)=>{
            console.log(errMsg);
        } );
    },
    onPullDownRefresh: function () {
        // 小程序提供的api，通知页面停止下拉刷新效果
        setTimeout(function () {
            wx.stopPullDownRefresh();
        }, 1000)
    },
    onLoad: function(options) {
        if(wx.getStorageSync('sessionid')==''){
            wx.login({
                success: function (r) {
                    app.login({code:r.code});
                }
            });
        }
        this.getCommisionData();
    }
});