//获取应用实例
const app = getApp();
app.getCommonTime();
Page({
  data: {
      reviewClassify:["全部(999+)","有图(220+)","追评(220+)","很好看(999+)","质量好(999+)","服务好(999+)"],
      reviewClassify1:["全部(0)"],
      currentTab:0,
      userData:[],
      userReviewData:[],
      orderCreateTime:[],
      scrollPageNum:1,
      proId:'',
      imgUrl:[],
      hasData:true,
      orderTotalPage:'',
      onLoading:false,
      noMoreData:false,
      bindDownLoad:false,
  },
    choiceReviewClassify:function(e) {
        const that = this;
        that.setData({
            currentTab:e.target.dataset.index
        });
    },
    onPullDownRefresh: function () {
        // 小程序提供的api，通知页面停止下拉刷新效果
        setTimeout(function () {
            wx.stopPullDownRefresh();
        }, 1000);
        this.getMoreReview(this.data.proId);
    },
    bindDownLoad:function(e){
        let that=this;
        that.setData({
            bindDownLoad:true
        });
        if(that.data.orderTotalPage==this.data.scrollPageNum){
            that.setData({
                noMoreData:true,
                onLoading:false,
            });
            return;
        }else{
            this.data.scrollPageNum++;
            that.setData({
                onLoading:true,
                scrollPageNum:this.data.scrollPageNum
            });
        }
        that.getMoreReview(this.data.proId);

    },
    refresh:function(e){
        let that=this;
        that.setData({
            scrollPageNum:1,
            bindDownLoad:false
        });
        that.getMoreReview(this.data.proId);
    },
    getMoreReview:function(proId){
        app.handlerPost('/api/goodsAppraises/page.do',{'page.page':this.data.scrollPageNum,userToken:wx.getStorageSync('sessionid'),proId:proId},"GET").then( (res)=>{
            if(res.data.code=='SUCCESS'){
                let data=res.data.data.items;
                if(data.length>0){
                    let createTime=[];
                    let imgs=[];
                    this.setData({
                        orderTotalPage:res.data.data.totalPage,
                        hasData:true
                    });
                    if(this.data.scrollPageNum==1){
                        this.setData({
                            userReviewData:data,
                        });
                    }else{
                        this.setData({
                            userReviewData:this.data.userReviewData.concat(data),
                        });
                    }
                    for(var i=0;i<this.data.userReviewData.length;i++){
                        createTime.push(  new Date(this.data.userReviewData[i].CREATE_TIME).Format("yyyy-MM-dd"));
                        imgs.push(this.data.userReviewData[i].IMGS.split(','))
                    }
                    this.setData({
                        orderCreateTime:createTime,
                        imgUrl:imgs
                    })
                }else{
                    this.setData({
                        hasData:false
                    });
                }

            }
        }).catch( (errMsg)=>{
            console.log(errMsg);
        } );
    },
    onShow:function(){

    },
    onLoad:function(options){
        if(wx.getStorageSync('sessionid')==''){
            wx.login({
                success: function (r) {
                    app.login({code:r.code});
                }
            });
        }
      this.getMoreReview(options.proId);
      wx.setStorageSync('proId',options.proId);
      this.setData({
          userData: wx.getStorageSync('myUserIInfo'),
          proId: wx.getStorageSync('proId')
      });
    },
});
