var app = getApp();
Page({
    data:{
        makeSureOrderData:[],
        addressId:''
    },
    onLoad: function(options) {
       this.setData({
           makeSureOrderData:JSON.parse(options.makeSureOrderData),
           addressId:options.addressId
       });
    },
    payment: function () {
        let options = this.data.makeSureOrderData;
        let openid = wx.getStorageSync('sessionid');
        wx.request({
            url: 'https://api.mzgf.com/api/product/doToBuyProduct.do',
            method: 'GET',
            header: {
                'content-type': "application/json",
            },
            data: {
                pdtId: options.pdtId,
                materialCode: options.materialCode,
                colourCode: options.colourCode,
                standardsCode: options.standCode,
                materialName: options.material,
                colourName: options.colour,
                standardsName: options.stand,
                num: options.cartNum,
                userToken: openid,
                addressId:this.data.addressId
            },
            success: function (res) {
                let data = res.data.data;
                if(res.data.code=='SUCCESS'){
                    wx.requestPayment({
                        'timeStamp': data.timeStamp,
                        'nonceStr': data.nonceStr,
                        'package': data.prepayId,
                        'signType': 'MD5',
                        'paySign': data.paySign,
                        'success': function (res) {
                            console.log(res);
                            wx.showToast({
                                title: '支付成功',
                                icon: 'success',
                                duration: 3000,
                                success:function(){
                                    app.handlerPost('/api/order/wechatAppNotify.do',{userToken:wx.getStorageSync('sessionid'),orderNo:data.orderNo,payStatus:0},"GET").then( (res)=>{
                                        // console.log(res);
                                    }).catch( (errMsg)=>{
                                        // console.log(errMsg);
                                    } );
                                    wx.navigateTo({
                                        url: '../paySuccess/paySuccess'
                                    });
                                }
                            });

                        },
                        'fail': function (res) {
                            wx.showModal({
                                title: '提示',
                                content:'付款失败',
                                success:function(res){
                                    if(res.confirm){
                                        wx.navigateTo({
                                            url: '../userOrder/userOrder'
                                        });
                                    }else if(res.cancel){

                                    }
                                },
                            });

                        },
                        'complete': function (res) {

                        }
                    })
                }else{
                    wx.showModal({
                        title: '提示',
                        content:res.data.msg,
                        success:function(res){
                            if(res.confirm){

                            }else if(res.cancel){

                            }
                        },
                    });
                    return;
                }

            },
            error: function () {

            }
        });

    },

});