var app = getApp();
Page({
    data:{
        imgUrls: [],
        hasData:false,
        commonUrl:app.globalData.commonUrl,
        getMyCollectData:[]
    },
    goGoodDetail:function(e){
        let id=e.currentTarget.dataset.id;
        wx.navigateTo({
            url: '../goodDetail/goodDetail?id='+id
        })
    },
    deleteCollectData:function(e){
        let item=e.currentTarget.dataset.item;
        let that=this;
        let collectParams={
            userToken:wx.getStorageSync('sessionid'),
            proId:item.proId,
            proType:1,
            title:item.title,
        };
        wx.showModal({
            title: '提示',
            content:'确定删除?',
            success:function(res){
                if(res.confirm){
                    app.handlerPost('/api/account/doRemoveMyFavorite.do',collectParams,"GET").then( (res)=>{
                        if(res.data.code=="SUCCESS"){
                            wx.showToast({
                                title: '删除成功',
                                icon: 'success',
                                duration: 2000
                            });
                            that.getMyCollectData();
                        }
                    }).catch( (errMsg)=>{
                        console.log(errMsg);
                    } );
                }else if(res.cancel){
                    console.log('已取消删除');
                }
            },
        });
    },
    getMyCollectData:function(){
        wx.showLoading({
            title: '加载中',
        });
        app.handlerPost('/api/account/doMyFavoritePage.do',{userToken:wx.getStorageSync('sessionid'),type:1},"GET").then( (res)=>{
            if(res.data.code=="SUCCESS"){
                let data=res.data.data.items;
                if(data.length>0){
                    this.setData({
                        hasData:true,
                        getMyCollectData:data
                    });
                }else{
                    this.setData({
                        hasData:false
                    });
                }
            }
            app.fetchAddressList();
        }).catch( (errMsg)=>{
            console.log(errMsg);
        } );
    },
    onShow:function(){
        this.getMyCollectData();
    },
    onLoad: function() {
        if(wx.getStorageSync('sessionid')==''){
            wx.login({
                success: function (r) {
                    app.login({code:r.code});
                }
            });
        }
       this.getMyCollectData();
    },

});