var app = getApp();
Page({
    data:{
        ConfigListData:[],
        commonUrl:app.globalData.commonUrl,
        title:{}
    },
    //美璟商城
    meijingScenery:function(){
        wx.switchTab({
            url:'../BeauSceneryShopCart/BeauSceneryShopCart'
        })
    },
    goGoodDetail:function(e){
        let saleStatus=e.currentTarget.dataset.status;
        let projectCode=e.currentTarget.dataset.code;
        if(saleStatus=='SALE'){
            wx.navigateTo({
                url: '../goodDetail/goodDetail?projectCode='+projectCode+'&type=code'
            })
        }else{
            wx.showModal({
                title:'该产品已失效',
                content:'无法查看',
                success:function(res){
                    if(res.confirm){
                        wx.switchTab({
                            url:'../BeauSceneryShopCart/BeauSceneryShopCart'
                        })
                    }else if(res.cancel){

                    }
                },
            });
        }
    },
    getConfigListData:function(projectCode){
        wx.showLoading({
            title: '加载中',
        });
        app.handlerPost('/api/plateForm/projectList.do',{projectCode:projectCode},"GET").then( (res)=>{
            if(res.data.code=="SUCCESS"){
                let data=res.data.data;
                this.setData({
                    ConfigListData:data
                });
            }
            app.fetchAddressList();
        }).catch( (errMsg)=>{
            console.log(errMsg);
        } );
        app.fetchAddressList();
    },
    getConfigTitle:function(projectCode){
        app.handlerPost('/api/plateForm/findProjectByCode.do',{projectCode:projectCode},"GET").then( (res)=>{
            console.log(res);
            if(res.data.code=="SUCCESS"){
                let data=res.data.data;
                this.setData({
                    title:data
                });
            }
            app.fetchAddressList();
        }).catch( (errMsg)=>{
            console.log(errMsg);
        } );
    },
    onPullDownRefresh: function () {
        // 小程序提供的api，通知页面停止下拉刷新效果
        setTimeout(function () {
            wx.stopPullDownRefresh();
        }, 1000)
    },
    onShareAppMessage: function (res) {
        if (res.from === 'button') {
            console.log(res.target)
        }
        return {
            title: '美璟小程序',
            path: '/pages/configList/configList?type=share&parentOpenId='+wx.getStorageSync('sessionid'),
            success: function (res) {
                console.log(res);
            },
            fail: function (res) {
                console.log(res);
            }
        }
    },
    onLoad: function(options) {
        this.getConfigListData(options.projectCode);
        this.getConfigTitle(options.projectCode);
        if(wx.getStorageSync('sessionid')==''){
            wx.login({
                success: function (r) {
                    if(options.type=='share'){
                        wx.setStorageSync('parentOpenId', options.parentOpenId);
                        app.login({code:r.code,parentOpenId:options.parentOpenId});
                    }else{
                        app.login({code:r.code});
                    }
                }
            });
        }
    }
});