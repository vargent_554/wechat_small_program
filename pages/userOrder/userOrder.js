var app = getApp();
Page({
    data: {
        navData: ["全部订单", "待付款", "已付款", "待发货", "待收货", "待评价"],
        orderStatus: ['SUCCESS', 'FAHUO', 'FINISH'],
        payStatus: ['', 'INIT', 'SUCCESS'],
        swiperItemData: [],
        userOrderData: [],
        winHeight: "", //窗口高度
        currentTab: 0, //预设当前项的值
        scrollLeft: 0, //tab标题的滚动条位置
        scrollPageNum: 1,
        orderTotalPage: '',
        onLoading: false,
        noMoreData: false,
        bindDownLoad: false,
        scrollTop: 0,
        hasDataShow: true,
        commonUrl: app.globalData.commonUrl,
    },
    //取消订单
    cancelOrder: function (e) {
        let orderid = e.currentTarget.dataset.id;
        let that = this;
        wx.showModal({
            title: '提示',
            content: '确认取消订单',
            success: function (res) {
                if (res.confirm) {
                    app.handlerPost('/api/order/doCancelOrder.do', {
                        userToken: wx.getStorageSync('sessionid'),
                        orderId: orderid
                    }, "GET").then((res) => {
                        if (res.data.code == "SUCCESS") {
                            wx.showToast({
                                title: '已取消订单',
                                icon: 'success',
                                duration: 3000,
                                success: function () {

                                }
                            });
                            that.getUserOrder();
                        }
                        app.fetchAddressList();
                    }).catch((errMsg) => {
                        console.log(errMsg);
                    });
                } else if (res.cancel) {}
            },
        })
    },
    //确认收货
    makeSureOrder: function (e) {
        let orderid = e.currentTarget.dataset.id;
        let that = this;
        wx.showModal({
            title: '提示',
            content: '确认收货',
            success: function (res) {
                if (res.confirm) {
                    app.handlerPost('/api/order/doSureOrder.do', {
                        userToken: wx.getStorageSync('sessionid'),
                        orderId: orderid
                    }, "GET").then((res) => {
                        if (res.data.code == "SUCCESS") {
                            wx.showToast({
                                title: '已确认订单',
                                icon: 'success',
                                duration: 3000,
                                success: function () {

                                }
                            });
                            that.getUserOrder();
                        }
                        app.fetchAddressList();
                    }).catch((errMsg) => {
                        console.log(errMsg);
                    });
                } else if (res.cancel) {}
            },
        })
    },
    singelGood: function () {
        wx.showToast({
            title: '提醒成功',
            icon: 'success',
            duration: 3000,
            success: function () {

            }
        });
    },
    goToPay: function (e) {
        let orderid = e.currentTarget.dataset.orderid;
        wx.showLoading({
            title: '加载中',
        });
        app.handlerPost('/api/wxpay/doToWxPay.do', {
            userToken: wx.getStorageSync('sessionid'),
            orderId: orderid
        }, "GET").then((res) => {
            if (res.data.code == "SUCCESS") {
                let data = res.data.data;
                wx.requestPayment({
                    'timeStamp': data.timeStamp,
                    'nonceStr': data.nonceStr,
                    'package': data.prepayId,
                    'signType': 'MD5',
                    'paySign': data.paySign,
                    'success': function (res) {
                        wx.showToast({
                            title: '支付成功',
                            icon: 'success',
                            duration: 3000,
                            success: function () {
                                wx.navigateTo({
                                    url: '../paySuccess/paySuccess'
                                });
                            }
                        });
                        app.handlerPost('/api/order/wechatAppNotify.do', {
                            userToken: wx.getStorageSync('sessionid'),
                            orderNo: data.orderNo,
                            payStatus: 0
                        }, "GET").then((res) => {
                            console.log(res);
                        }).catch((errMsg) => {
                            console.log(errMsg);
                        });

                    },
                    'fail': function (res) {
                        wx.showModal({
                            title: '提示',
                            content: '付款失败',
                            success: function (res) {
                                if (res.confirm) {

                                } else if (res.cancel) {
                                    wx.navigateTo({
                                        url: '../userOrder/userOrder'
                                    });
                                }
                            },
                        });

                    },
                    'complete': function (res) {

                    }
                });
                app.fetchAddressList();
            } else {
                let msg = res.data.msg;
                app.fetchAddressList();
                wx.showModal({
                    title: '提示',
                    content: msg,
                    success: function () {},
                })
            }
        }).catch((errMsg) => {
            console.log(errMsg);
        });
    },
    // 滚动切换标签样式
    switchTab: function (e) {
        let currentData = e.detail.current;
        this.setData({
            currentTab: currentData
        });
        this.setData({
            scrollPageNum: 1,
            bindDownLoad: false
        });
        this.checkCor();
        this.getUserOrder();
    },
    // 点击标题切换当前页时改变样式
    swichNav: function (e) {
        let cur = e.target.dataset.current;
        if (this.data.currentTab == cur) {
            return false;
        } else {
            this.setData({
                currentTab: cur
            })
        }
        this.setData({
            scrollPageNum: 1,
            bindDownLoad: false
        });
        this.getUserOrder();
    },
    //判断当前滚动超过一屏时，设置tab标题滚动条。
    checkCor: function () {
        if (this.data.currentTab > 3) {
            this.setData({
                scrollLeft: 300
            })
        } else {
            this.setData({
                scrollLeft: 0
            })
        }
    },
    onPullDownRefresh: function () {
        // 小程序提供的api，通知页面停止下拉刷新效果
        setTimeout(function () {
            wx.stopPullDownRefresh();
        }, 1000)
    },
    goOrderDetail: function (e) {
        let id = e.currentTarget.dataset.currentid;
        if (e.currentTarget.dataset.iscart == 'CARD') {
            return;
        } else {
            wx.navigateTo({
                url: '../orderDetail/orderDetail?goPay=0&orderId=' + id
            });
        }
    },
    bindDownLoad: function (e) {
        let that = this;
        that.setData({
            bindDownLoad: true
        });
        if (that.data.orderTotalPage == this.data.scrollPageNum) {
            that.setData({
                noMoreData: true,
                onLoading: false,
            });
            return;
        } else {
            this.data.scrollPageNum++;
            that.setData({
                onLoading: true,
                scrollPageNum: this.data.scrollPageNum
            });
        }
        that.getUserOrder();

    },
    refresh: function (e) {
        wx.showNavigationBarLoading();
        let that = this;
        that.setData({
            scrollPageNum: 1,
            bindDownLoad: false
        });
        that.getUserOrder();
    },
    getUserOrder: function () {
        let orderStatusData = '',
            payStatusData = '';
        if (this.data.currentTab > 2) {
            orderStatusData = this.data.orderStatus[this.data.currentTab - 3]
        } else {
            payStatusData = this.data.payStatus[this.data.currentTab]

        }
        wx.showLoading({
            title: '加载中',
        });
        app.handlerPost('/api/account/doMyOrderPage.do', {
            'page.page': this.data.scrollPageNum,
            'userToken': wx.getStorageSync('sessionid'),
            'payStatus': payStatusData,
            'orderStatus': orderStatusData
        }, "GET").then((res) => {
            if (res.data.code == "SUCCESS") {
                let data = res.data.data.items;
                this.setData({
                    orderTotalPage: res.data.data.totalPage
                });
                if (this.data.scrollPageNum == 1) {
                    this.setData({
                        userOrderData: data,
                    });
                } else {
                    this.setData({
                        userOrderData: this.data.userOrderData.concat(data),
                    });
                }
                if (data.length > 0) {
                    this.setData({
                        hasDataShow: true
                    });
                } else {
                    this.setData({
                        hasDataShow: false
                    });
                }
                setTimeout(function () {
                    wx.hideNavigationBarLoading();
                    wx.stopPullDownRefresh();
                }, 1000);
            }
            app.fetchAddressList();
        }).catch((errMsg) => {
            console.log(errMsg);
        });
    },
    goEvaluate: function (e) {
        let items = JSON.stringify(e.currentTarget.dataset.items);
        wx.navigateTo({
            url: '../evaluate/evaluate?items=' + items
        })
    },
    onShow: function () {
        var that = this;
        //  高度自适应
        wx.getSystemInfo({
            success: function (res) {
                var clientHeight = res.windowHeight,
                    clientWidth = res.windowWidth,
                    rpxR = 750 / clientWidth;
                var calc = clientHeight * rpxR;
                that.setData({
                    winHeight: calc
                });
            }
        });
    },
    onLoad: function (option) {
        console.log(option)
        if (wx.getStorageSync('sessionid') == '') {
            wx.login({
                success: function (r) {
                    app.login({
                        code: r.code
                    });
                }
            });
        }
        this.getUserOrder();
        for (var i = 0; i < this.data.navData.length; i++) {
            this.data.swiperItemData.push(i);
        }
        this.setData({
            swiperItemData: this.data.swiperItemData
        });
    },
    footerTap: app.footerTap
});