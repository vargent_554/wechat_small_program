var app = getApp();
Page({
    data:{
        choiceN:false,
        choiceStar:4,
        uploadImg:[],
        productData:[],
        commonUrl:app.globalData.commonUrl,
        uploadImgData:[]
    },
    choiceAnonEva:function(){
       this.setData({
           choiceN:!this.data.choiceN
       })
    },
    choiceFullStar:function(e){
        var that=this;
        var index=e.target.dataset.index;
        that.setData({
            choiceStar:index
        })

    },
    //上传图片
    chooseImage:function(e){
        var that = this;
        wx.chooseImage({
            count:3,
            sizeType: ['original', 'compressed'],  //可选择原图或压缩后的图片
            sourceType: ['album', 'camera'], //可选择性开放访问相册、相机
            success: res => {
                var tempFilePaths = res.tempFilePaths;
                var uploadImgs = that.data.uploadImg;
                for (var i = 0; i < tempFilePaths.length; i++) {
                    if(uploadImgs.length>2){
                        wx.showModal({
                            title: '提示',
                            icon:'error',
                            content: '不能超过三张图片'
                        })
                    }else{
                        uploadImgs.push(tempFilePaths[i]);
                    }
                }
                that.setData({
                    uploadImg: uploadImgs
                });
            }
        });
    },
    //删除照片
    deleteImg: function (e) {
        var uploadImgs = this.data.uploadImg;
        var index = e.currentTarget.dataset.index;
        uploadImgs.splice(index, 1);
        this.setData({
            uploadImg: uploadImgs
        });
    },
    //预览图片
    previewImg: function (e) {
        var index = e.target.dataset.index;
        var uploadImgs = this.data.uploadImg;
        wx.previewImage({
            //当前显示图片
            current: uploadImgs[index],
            //所有图片
            urls: uploadImgs
        })
    },
    uploadUserEvaluate:function(content,img){
        let uploadEvaluateParams={
            userToken:wx.getStorageSync('sessionid'),
            proId:this.data.productData.pdtId,
            orderId:this.data.productData.id,
            proType:this.data.productData.pdtType,
            goodsScore:this.data.choiceStar+1,
            contnt:content,
            nameFlag:this.data.choiceN?'1':'0',
            imgs:img
        };
        app.handlerPost('/api/goodsAppraises/doAdd.do',uploadEvaluateParams,"GET").then( (res)=>{
            if(res.data.code=='SUCCESS'){
                wx.showToast({
                    title: '评价成功',
                    icon: 'success',
                    duration: 2000
                });
                wx.navigateTo({
                    url: '../useReview/useReview?proId='+this.data.productData.pdtId
                })
            }else{
                wx.showToast({
                    title: '此订单已评论',
                    icon: 'success',
                    duration: 2000
                });
                wx.navigateTo({
                    url: '../useOrder/userOrder'
                })
            }
        }).catch( (errMsg)=>{
            console.log(errMsg);
        } );
    },
    formSubmit: function(e) {
        let that=this,warn = "",flag = true;
        let tempFilePaths=this.data.uploadImg;
        var uploadImgData=[];
        var url='https://mzgf.oss-cn-hangzhou.aliyuncs.com';
        ;var tempLen=tempFilePaths.length;
        let content=e.detail.value.shareEvalute;
        if(that.data.choiceStar<0){
            warn = "请输入评分";
        }else if(content==''){
            warn = "请输入分享";
        }else{
            flag=false;
            if(tempFilePaths.length>0){
                for (var i = 0; i < tempFilePaths.length; i++) {
                    var fileName = tempFilePaths[i].split('/')[3];
                    // 要提交的key
                    var fileKey = 'upload/'+ fileName;
                    wx.uploadFile({
                        url:url ,
                        filePath: tempFilePaths[i],
                        name: 'file',
                        formData: {
                            name: tempFilePaths[i],
                            key: fileKey,
                            policy: 'eyJleHBpcmF0aW9uIjoiMjAyMC0wMS0wMVQxMjowMDowMC4wMDBaIiwiY29uZGl0aW9ucyI6W1siY29udGVudC1sZW5ndGgtcmFuZ2UiLDAsMTA0ODU3NjAwMF1dfQ==',
                            OSSAccessKeyId: 'LTAI6wCzJYXnxvjF',
                            signature: 'S6R6lMeR69PLPSsiLfuXGkbhmpc=',
                            success_action_status: "200"
                        },
                        success: function (res) {
                            if(res.errMsg=="uploadFile:ok"){
                                uploadImgData.push(url+fileKey);
                                if(tempLen==uploadImgData.length){
                                    let img=uploadImgData.join(',');
                                    that.uploadUserEvaluate(content,img);
                                }
                            }
                        }
                    })
                }
            }else{
                that.uploadUserEvaluate(content,'');
            }

        };
        if(flag==true) {
            wx.showModal({
                title: '提示',
                content: warn
            })
        }

    },
    onLoad:function(options){
        if(wx.getStorageSync('sessionid')==''){
            wx.login({
                success: function (r) {
                    app.login({code:r.code});
                }
            });
        }
       let productData=JSON.parse(options.items);
       this.setData({
           productData:productData
       });
    }
});