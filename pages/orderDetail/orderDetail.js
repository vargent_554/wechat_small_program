//获取应用实例
const app = getApp();
app.getCommonTime();
Page({
    data: {
        orderDetailInfo:{},
        orderTime:'',
        goPay:0,
        orderId:'',
        commonUrl:app.globalData.commonUrl,
    },
    getOrderDetail:function(options){
        wx.showLoading({
            title: '加载中',
        });
        app.handlerPost('/api/account/doMyOrderDetail.do',{userToken:wx.getStorageSync('sessionid'),orderId:options.orderId},"GET").then( (res)=>{
            if(res.data.code=="SUCCESS"){
                let data=res.data.data;
                app.fetchAddressList();
                this.setData({
                    orderDetailInfo:data,
                    orderTime:data.createTime!=''?new Date(data.createTime).Format("yyyy-MM-dd hh:mm:ss"):''
                });

            }
        }).catch( (errMsg)=>{
            console.log(errMsg);
        } );
    },
    onLoad:function(options){
        if(wx.getStorageSync('sessionid')==''){
            wx.login({
                success: function (r) {
                    app.login({code:r.code});
                }
            });
        }
       this.getOrderDetail(options);
       this.setData({
           goPay:options.goPay,
           orderId:options.orderId
       });
    }
});