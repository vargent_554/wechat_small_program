var app = getApp();
Page({
    data:{
        indicatorDots: true, //是否显示面板指示点
        autoplay: true, //是否自动切换
        interval: 2000, //自动切换时间间隔,3s
        duration: 800, //  滑动动画时长1s
        broadData:[],
        pdtBrand:{},
        imgUrl:[],
        commonUrl:app.globalData.commonUrl,
    },
    onPullDownRefresh: function () {
        // 小程序提供的api，通知页面停止下拉刷新效果
        setTimeout(function () {
            wx.stopPullDownRefresh();
        }, 1000)
    },
    onShareAppMessage: function (res) {
        if (res.from === 'button') {
            console.log(res.target)
        }
        return {
            title: '美璟小程序',
            path: '/pages/qixiapinpai1/qixiapinpai1?type=share&parentOpenId='+wx.getStorageSync('sessionid'),
            success: function (res) {
                console.log(res);
            },
            fail: function (res) {
                console.log(res);
            }
        }
    },
    getBroadData:function(options){
        wx.showLoading({
            title: '加载中',
        });
        app.handlerPost('/api/product/doGetPdtByBCode.do',{userToken:'',brandCode:options.brandCode},"GET").then( (res)=>{
            if(res.data.msg=="SUCCESS"){
                app.fetchAddressList();
              if (res.data.data.pdtBrand.imageUrls != null){
                this.setData({
                  broadData: res.data.data.products.items,
                  pdtBrand: res.data.data.pdtBrand,
                  imgUrl: res.data.data.pdtBrand.imageUrls.split(',')
                });
              }else{
                this.setData({
                  broadData: res.data.data.products.items,
                  pdtBrand: res.data.data.pdtBrand,
                  imgUrl: null
                });
              }
            }
        }).catch( (errMsg)=>{
            console.log(errMsg);
        } );
    },
    goGoodDetail:function(e){
        let id=e.currentTarget.dataset.id;
        wx.navigateTo({
            url: '../goodDetail/goodDetail?id='+id
        })
    },
    onLoad: function(options) {
        wx.login({
            success: function (r) {
                if(options.type=='share'){
                    wx.setStorageSync('parentOpenId', options.parentOpenId);
                    app.login({code:r.code,parentOpenId:options.parentOpenId});
                }else{
                    app.login({code:r.code});
                }
            }
        });
        this.getBroadData(options);
    },

});