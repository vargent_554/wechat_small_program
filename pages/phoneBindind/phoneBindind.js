// pages/phoneBindind/phoneBindind.js
Page({
  
  /**
   * 页面的初始数据
   */
  data: {
    formData: {
      phone: null,
    },
    userPhone: null,
    alertStatus:true,    
  },
  updateFormData: function (e) {
    let o = this.data.formData;
    o[e.currentTarget.dataset.key] = e.detail.value;
    this.setData({
      formData: o,
    })
    if (this.data.formData.phone || this.data.userPhone==''){
      this.setData({
        alertStatus: false,
      })
    }
  },
  submit:function(){
    // wx.showLoading({
    //   title: '加载中',12
    // });
   var phonetest = /^1(3|4|5|7|8)\d{9}$/;
   var xin = this.data.formData.phone;
   if (!phonetest.test(xin)){
     wx.showToast({
       title: '输入的手机号有误',
       icon: 'none'
     })
   }else{
     if (this.data.userPhone == this.data.formData.phone){
       this.setData({
         alertStatus: true,
       })
          } else {
            wx.request({
              method: 'GET',
              data: this.data.formData,
              header: {
                'content-type': 'application/x-www-form-urlencodeds'
              },
              url: 'https://api.mzgf.com/api/account/doBindMobile.do',
              success: res => {
              wx.switchTab({
              url: '../PersonalCenter/PersonalCenter'
              });
            }
          })
         } 
    }      
},
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options)
    this.setData({
      userPhone: options.data
    })
   
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
   
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
   
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})