var app = getApp();
function transmit(data,value){
    if(data==null){
        return '';
    }else{
        return data[value]
    }
}
Page({
    data:{
        currentTab:0, //预设当前项的值
        imgUrls: [],
        indicatorDots: true, //是否显示面板指示点
        autoplay: true, //是否自动切换
        interval: 2000, //自动切换时间间隔
        duration: 300, //  滑动动画时长1s
        showDialog:false,
        cartNum:1,
        choiceTip:false,
        currentTab1:0,
        currentTab2:0,
        currentTab3:0,
        collectGoodStatus:false,
        standardCode:[],
        colourCode:[],
        materialCode:[],
        goodStandardCode:[],
        goodMaterialCode:[],
        goodColourCode:[],
        goodDetailTotalData:[],
        getPdtSkuPrice:'',
        commonUrl:app.globalData.commonUrl,
        pdtId:'',
        choiceShopType:'',
        shopCartNum:0,
        standNameS:'',
        materialNameS:'',
        colorNameS:'',
        showStand:false,

    },
    collectGood:function(){
        this.setData({
            collectGoodStatus:!this.data.collectGoodStatus
        });
        if(this.data.collectGoodStatus){
            this.addCollection();
        }else{
            this.deleteCollection();
        }
    },
    getCartNum:function(){
        app.handlerPost('/api/shopCart/doMyCart.do',{userToken:wx.getStorageSync('sessionid')},"GET").then( (res)=> {
            if (res.data.code == 'SUCCESS') {
                let data = res.data.data,cartNum=0;
                this.setData({shopCartNum:data.length});
            }
        })
    },
    getProductGoodInfo:function(options){
        let that=this;
        wx.showLoading({
            title: '加载中',
        });
        app.handlerPost('/api/product/doGetPdtById.do',{userToken:wx.getStorageSync('sessionid'),pdtId:options.id},"GET").then( (res)=>{
            if(res.data.msg=="SUCCESS"){
                setTimeout(res => {
                    wx.hideLoading();
                }, 500);
                let goodDetailTotalData=res.data.data;
                let  imgUrls=res.data.data.imageUrls.split(',');
                function filter(data){
                    if(data!=null){
                        return data.split("#");
                    }else{
                        data=''
                    }
                }
                this.setData({
                    goodDetailTotalData:goodDetailTotalData,
                    standardCode:filter(goodDetailTotalData.standardCode),
                    colourCode:filter(goodDetailTotalData.colourCode),
                    materialCode:filter(goodDetailTotalData.materialCode),
                    goodStandardCode:filter(goodDetailTotalData.standardName),
                    goodColourCode:filter(goodDetailTotalData.colourName),
                    goodMaterialCode:filter(goodDetailTotalData.materialName),
                    imgUrls:imgUrls,
                    getPdtSkuPrice:goodDetailTotalData.price,
                });
                if(this.data.goodDetailTotalData.isCollect=='T'){
                    this.setData({
                        collectGoodStatus:true,
                    });
                }
                this.getGoodAttr();

            }
        }).catch( (errMsg)=>{
            console.log(errMsg);
        } );
    },
    onShareAppMessage: function (res) {
        if (res.from === 'button') {
            console.log(res.target)
        }
        return {
            title: '美璟小程序',
            path: '/pages/goodDetail/goodDetail?type=share&parentOpenId='+wx.getStorageSync('sessionid')+'&id='+this.data.goodDetailTotalData.id,
            success: function (res) {
                console.log(res);
            },
            fail: function (res) {
                console.log(res);
            }
        }
    },
    getGoodAttr:function(){
        function transmit(data,value){
            if(data==null){
                return '';
            }else{
                return data[value]
            }
        }
        wx.showLoading({
            title: '加载中',
        });
        app.handlerPost('/api/product/doGetPdtSkuPrice.do',{userToken:wx.getStorageSync('sessionid'),materialCode:transmit(this.data.materialCode,this.data.currentTab2),standardCode:transmit(this.data.standardCode,this.data.currentTab3),colourCode:transmit(this.data.colourCode,this.data.currentTab1),pdtId:this.data.goodDetailTotalData.id},"GET").then( (res)=>{
            if(res.data.msg=="SUCCESS"){
                app.fetchAddressList();
                this.setData({
                    getPdtSkuPrice:res.data.data
                });
            }
            if(res.data.data==null){
                app.fetchAddressList();
                this.setData({
                    getPdtSkuPrice:{
                        imageUrl:null,
                        price:'已售完'
                    }
                });
            }
         }).catch( (errMsg)=>{
            console.log(errMsg);
        } );

    },
    goShoppingCart:function(){
        wx.switchTab({
            url:'../ShoppingCart/ShoppingCart',
            success:function(){
            },
        });
    },
    getBrandCode:function(){
        wx.navigateTo({
            url: '../qixiapinpai1/qixiapinpai1?brandCode='+this.data.goodDetailTotalData.brandCode
        })
    },
    getProductCodeId:function(code){
        let that=this;
        app.handlerPost('/api/product/doGetPdtIdByPdtCode.do',{pdtCode:code},"GET").then( (res)=>{
            if(res.data.code='SUCCESS'){
                let data=res.data.data;
                that.getProductGoodInfo({'id':data});
            }

        }).catch( (errMsg)=>{
            console.log(errMsg);
        } );
    },
    onShow:function(){

    },
    onLoad: function(options) {
        wx.login({
            success: function (r) {
                if(options.type=='share'){
                    wx.setStorageSync('parentOpenId', options.parentOpenId);
                    console.log();
                    app.login({code:r.code,parentOpenId:options.parentOpenId});
                }else{
                    app.login({code:r.code});
                }
            }
        });
        if(options.type=='code'){
            let code=options.projectCode;
            this.getProductCodeId(code);
        }else{
            this.getProductGoodInfo(options);
        }
        this.getCartNum();
    },
    toMemberServerDetail:function(){
        wx.navigateTo({
            url: '../memberServiceDetail/memberServiceDetail'
        })
    },
    getGoodAttribute:function(e){
        const that = this;
        that.setData({
            currentTab1:e.target.dataset.index
        });
        this.getGoodAttr();
    },
    getGoodAttribute1:function(e){
        const that = this;
        that.setData({
            currentTab2:e.target.dataset.index
        });
        this.getGoodAttr();
    },
    getGoodAttribute2:function(e){
        const that = this;
        that.setData({
            currentTab3:e.target.dataset.index
        });
        this.getGoodAttr();
    },
    swichNav5:function(e){
        var cur=e.target.dataset.current;
        if(this.data.currentTab==cur){
            return false;
        } else{
            this.setData({
                currentTab:cur
            })
        }
    },
    addShopCart:function(e){
     let goodStandardCode=this.data.goodStandardCode,
            goodColourCode=this.data.goodColourCode,
            goodMaterialCode=this.data.goodMaterialCode;
     let currettype=e.currentTarget.dataset.current;
     let standardStatus=goodStandardCode==null&goodColourCode==null&goodMaterialCode==null;
     let standardStatus1=getCodeLength(goodStandardCode)<=1&&getCodeLength(goodColourCode)<=1&&getCodeLength(goodMaterialCode)<=1;
     if(currettype=='buy'){
         this.setData({
             choiceShopType:'doBuy'
         });
     }else if(currettype=='addCart'){
         this.setData({
             choiceShopType:'doAddCart'
         });
     }else if(currettype=='choiceStandard'){
         this.setData({
             choiceShopType:'doChoiceStandard'
         });
     }
     function getCodeLength(data){
         if(data==null){
             return 0;
         }else{
             return data.length
         }
     }
     if(standardStatus||standardStatus1){
         this.setData({
             showDialog:false
         });
         if(currettype=='buy'){
             this.goBuyGood();
         }else if(currettype=='addCart'){
             this.addGoodToCart('addCart');
         }
     }else if(this.data.showStand&&currettype == 'buy'){
         this.setData({
             showDialog:false
         });
         this.goBuyGood();
     }else {
         this.setData({
             showDialog: !this.data.showDialog
         })
        }
    },
    addGoodCartOrBuyGood:function(e){
        let dotype=e.currentTarget.dataset.type;
        if(dotype=='doAddCart'){
            this.addGoodToCart();
        }else if(dotype=='doBuy'||dotype=='doChoiceStandard'){
            this.showBuyGoodInfo();
        }
        this.getCartNum();
    },
    addGoodToCart:function(info){
        if(info=='addCart'){
            this.setData({
                showDialog:false
            });
        }else{
            this.setData({
                showDialog:!this.data.showDialog
            });
        }
        let addCartParams={
            userToken:wx.getStorageSync('sessionid'),
            goodId:this.data.goodDetailTotalData.id,
            amount:this.data.cartNum,
            code1: transmit(this.data.standardCode,this.data.currentTab3),
            code2:transmit(this.data.materialCode,this.data.currentTab2) ,
            code3:transmit(this.data.colourCode,this.data.currentTab1)  ,
            name1:transmit(this.data.goodStandardCode,this.data.currentTab3),
            name2: transmit(this.data.goodMaterialCode,this.data.currentTab2),
            name3:  transmit(this.data.goodColourCode,this.data.currentTab1),
            pdtName:this.data.goodDetailTotalData.name,
            pdtImg:this.data.goodDetailTotalData.imageUrl,
            price:this.data.getPdtSkuPrice.price
        };
        app.handlerPost('/api/shopCart/doAddMyCart.do',addCartParams,"GET").then( (res)=>{
            if(res.data.code=="SUCCESS"){
                this.getCartNum();
                wx.showToast({
                    title: '成功加入购物车',
                    icon: 'success',
                    duration: 2000
                })
            }else{
                wx.showModal({
                    title:'该产品已失效',
                    content:'无法加入购物车',
                    success:function(res){
                        if(res.confirm){

                        }else if(res.cancel){

                        }
                    },
                });
            }
        }).catch( (errMsg)=>{
            console.log(errMsg);
        } );

    },
    goBuyGood:function(){
        this.setData({
            showDialog:false
        });
        let makeSureOrder={
            pdtId: this.data.goodDetailTotalData.id,
            stand: transmit(this.data.goodStandardCode,this.data.currentTab3),
            material:transmit(this.data.goodMaterialCode,this.data.currentTab2),
            colour: transmit(this.data.goodColourCode,this.data.currentTab1),
            cartNum: this.data.cartNum,
            price: this.data.getPdtSkuPrice.price,
            name: this.data.goodDetailTotalData.name,
            imgUrl: this.data.goodDetailTotalData.imageUrl,
            standCode:transmit(this.data.standardCode,this.data.currentTab3),
            materialCode: transmit(this.data.materialCode,this.data.currentTab2),
            colourCode: transmit(this.data.colourCode,this.data.currentTab1),
        };

        makeSureOrder=JSON.stringify(makeSureOrder);
        wx.navigateTo({
            url: '../makeSureOrder/makeSureOrder?makeSureOrder='+makeSureOrder+'&goodDetailType=1'
        })
    },
    showBuyGoodInfo:function(info){
        if(info=='buy'){
            this.setData({
                showDialog:false
            });
        }else{
            this.setData({
                showDialog:!this.data.showDialog
            });
        }
        if(this.data.getPdtSkuPrice.price=='已售完'){
            wx.showModal({
                content:'产品已售完,无法购买',
                success:function(res){
                    if(res.confirm){

                    }else if(res.cancel){

                    }
                },
            });
            return;
        }
        this.setData({
            standNameS: transmit(this.data.goodStandardCode,this.data.currentTab3),
            materialNameS: transmit(this.data.goodMaterialCode,this.data.currentTab2),
            colorNameS: transmit(this.data.goodColourCode,this.data.currentTab1),
            showStand:true,
        });
    },
    addCollection:function(){
        let collectParams={
            userToken:wx.getStorageSync('sessionid'),
            proId:this.data.goodDetailTotalData.id,
            proType:1,
            title:this.data.goodDetailTotalData.brandName+'   |  '+this.data.goodDetailTotalData.name,
        };
        app.handlerPost('/api/account/doJoinMyFavorite.do',collectParams,"GET").then( (res)=>{
            if(res.data.code=="SUCCESS"){
                wx.showToast({
                    title: '收藏成功',
                    icon: 'success',
                    duration: 2000
                })
            }
        }).catch( (errMsg)=>{
            console.log(errMsg);
        } );
    },
    deleteCollection:function(){
        let collectParams={
            userToken:wx.getStorageSync('sessionid'),
            proId:this.data.goodDetailTotalData.id,
            proType:1,
            title:this.data.goodDetailTotalData.brandName+'|'+this.data.goodDetailTotalData.name,
        };
        app.handlerPost('/api/account/doRemoveMyFavorite.do',collectParams,"GET").then( (res)=>{
            if(res.data.code=="SUCCESS"){
                wx.showToast({
                    title: '取消成功',
                    icon: 'success',
                    duration: 2000
                })
            }
        }).catch( (errMsg)=>{
            console.log(errMsg);
        } );
    },
    onPullDownRefresh: function () {
        // 小程序提供的api，通知页面停止下拉刷新效果
        setTimeout(function () {
            wx.stopPullDownRefresh();
        }, 1000)
    },
    minusCount:function(e){
        if(this.data.cartNum<=1){
            return false;
        }
        this.data.cartNum = parseInt(this.data.cartNum) - 1;
        this.setData({
            cartNum: this.data.cartNum
        });
    },
    addCount:function(e){
        this.data.cartNum = parseInt(this.data.cartNum) +1;
        this.setData({
            cartNum: this.data.cartNum
        });
    },
    bindManual:function(e){
        this.data.cartNum = e.detail.value;
        // 将数值与状态写回
        this.setData({
            cartNum: parseInt(this.data.cartNum)
        });
    }
});