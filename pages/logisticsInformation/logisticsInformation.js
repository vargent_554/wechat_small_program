var app = getApp();
app.getCommonTime();
Page({
    data:{
        logInforData:'',
        hasData:false,
        payTime:'',
        logisticsMsg:[]
    },
    getLogInformData:function(data){
        wx.showLoading({
            title: '加载中',
        });
        app.handlerPost('/api/account/doMyOrderDetail.do',{userToken:wx.getStorageSync('sessionid'),orderId:data},"GET").then( (res)=>{
            app.fetchAddressList();
            if(res.data.code=='SUCCESS'){
                if(res.data.data!=''||null){
                    this.setData({
                        logInforData:res.data.data,
                        payTime:res.data.data.payTime!=''?new Date(res.data.data.payTime).Format("yyyy-MM-dd hh:mm:ss"):'',
                        hasData:true,
                        logisticsMsg:JSON.parse(res.data.data.logisticsMsg)
                    });
                }else{
                   this.setData({
                       hasData:false
                   });
                }

            }

        }).catch( (errMsg)=>{
            console.log(errMsg);
        } );
    },
    onLoad:function(options){
        if(wx.getStorageSync('sessionid')==''){
            wx.login({
                success: function (r) {
                    app.login({code:r.code});
                }
            });
        }
      this.getLogInformData(options.id);
    }
});