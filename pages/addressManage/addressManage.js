const app = getApp();
Page({
    data: {
        addressList: [],
        choiceAddress:'',
        takeFromType:'',
        hasData:true
    },
    getUserAddressInfo:function(){
        wx.showLoading({
            title: '加载中',
        });
        app.handlerPost('/api/account/doMyUserAddress.do',{userToken:wx.getStorageSync('sessionid')},"GET").then( (res)=>{
            if(res.data.msg=="SUCCESS"){
                let data=res.data.data,addressData,addressDataArr=[];
                if(data.length>0){
                    this.setData({ hasData: true });
                }else{
                    this.setData({ hasData: false });
                }
                for(var i=0;i<data.length;i++){
                    addressData = {
                        id:data[i].id,
                        address:data[i].address,
                        addressdDetail:data[i].area1+data[i].area2+data[i].area2,
                        buyerPhone: data[i].userPhone,
                        isdefault: data[i].defaultAddress,
                        name: data[i].userName,
                        province: data[i].code1,
                        city: data[i].code2,
                        region: data[i].code3
                    };
                    addressDataArr.push(addressData);
                }
                this.setData({ addressList: addressDataArr });
            }
           this.fetchAddressList();
        }).catch( (errMsg)=>{
            console.log(errMsg);
        } );
    },
    onShow: function (options) {
        this.getUserAddressInfo();
    },
    fetchAddressList() {
        setTimeout(res => {
            wx.hideLoading();
        }, 500);

    },
    choiceAddressMana:function(e){
        let choiceAddressData=JSON.stringify(e.currentTarget.dataset.addressdata);
        if(this.data.typeAddress=='personcenter'){
            return;
        }else if(this.data.typeAddress=='makeorder'){
            wx.redirectTo({
                url: '../makeSureOrder/makeSureOrder?choiceAddressData='+choiceAddressData+'&choiceAddressType=1',
            })
        }

    },
    pushToEdit(e) {
        let addressData = JSON.stringify(e.currentTarget.dataset.addressdata);
        let id =e.currentTarget.dataset.id;
        wx.navigateTo({
            url: '../addNewAddress/addNewAddress?type=1&addressData=' + addressData+'&navbartitle=编辑地址&id='+id+'&typeAddress='+this.data.typeAddress,
        })
    },
    bindAddressAdd(e) {
        wx.navigateTo({
            url: '../addNewAddress/addNewAddress?type=0&navbartitle=新增地址&typeAddress='+this.data.typeAddress,
        })
    },
    onLoad:function(options){
        if(wx.getStorageSync('sessionid')==''){
            wx.login({
                success: function (r) {
                    app.login({code:r.code});
                }
            });
        }
        this.setData({
            typeAddress:options.typeAddress
        });
        if(options.choiceAddress==1){
            this.setData({
                choiceAddress:1
            });
        }
    }
});