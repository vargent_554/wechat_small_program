const app = getApp();
Page({
    data: {
        showVrProject : ''
    },

    onShareAppMessage: function (res) {
        if (res.from === 'button') {
            console.log(res.target)
        }
        return {
            title: '深度体验全球好房',
            path: '/pages/vrPageExhibt/vrPageExhibt?type=share&parentOpenId=' + wx.getStorageSync('sessionid') + '&outUrl=' + res.webViewUrl.split('?')[0],
            imageUrl: 'https://vr2018.oss-cn-beijing.aliyuncs.com/vr/shareimg/' + res.webViewUrl.split('/')[4]+'.jpg' ,
            success: function (res) {
                console.log(res);    
            },
            fail: function (res) {
                console.log(res);
            }
        }
    },
    onLoad: function (options) {
        wx.login({
            success: function (r) {
                if (options.type == 'share') {
                    wx.setStorageSync('parentOpenId', options.parentOpenId);
                    app.login({
                        code: r.code,
                        parentOpenId: options.parentOpenId
                    });
                } else {
                    app.login({
                        code: r.code
                    });
                }
            }
        });
        this.setData({
            showVrProject : options.outUrl
        })
    }

});