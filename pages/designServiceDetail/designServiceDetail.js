var app = getApp();
Page({
    data:{
        swiperList:[
            "../../images/example.png",
            "../../images/example.png",
            "../../images/example.png",
            "../../images/example.png"
        ],
        curIndex:0,
        winWidth:492,
        winHeight:700,
        allWidth:492*5,
        itemWidth:300
    },
    //触摸开始的事件
    swiperTouchstart: function (e) {
        // console.log('touchstart',e);
        let startClinetX = e.changedTouches[0].clientX;
        this.setData({
            startClinetX: startClinetX, //触摸开始位置；
            startTimestamp: e.timeStamp, //触摸开始时间；
        })
    },

    //触摸移动中的事件
    swiperTouchmove: function (e) {
        // console.log('touchmove',e);
    },

    //触摸结束事件
    swiperTouchend: function (e) {
        // console.log("触摸结束",e);

        let times = e.timeStamp - this.data.startTimestamp, //时间间隔；
            distance = e.changedTouches[0].clientX - this.data.startClinetX; //距离间隔；
        //判断
        if (times < 500 && Math.abs(distance) > 10) {
            let curIndex = this.data.curIndex;

            let x0 = this.data.itemWidth,x1 = this.data.translateDistance,x = 0;
            if ( distance > 0) {

                curIndex = curIndex - 1
                if(curIndex < 0){
                    curIndex = 0;
                    x0 = 0;
                }
                x = x1 + x0;
            } else {

                // console.log('+1',x);
                curIndex = curIndex + 1
                if (curIndex >= this.data.swiperList.length) {
                    curIndex = this.data.swiperList.length-1;
                    x0 = 0;
                }
                x = x1 - x0;
            }
            this.animationToLarge(curIndex, x);
            this.animationToSmall(curIndex, x);
            this.setData({
                curIndex: curIndex,
                translateDistance: x
            })

        } else {

        }
    },
    // 动画
    animationToLarge: function (curIndex,x) {

        this.animation.translateX(x).scale(1).step()
        this.setData({
            animationToLarge: this.animation.export()
        })
    },
    animationToSmall: function (curIndex,x) {

        this.animation.translateX(x).scale(0.7).step()
        this.setData({
            animationToSmall: this.animation.export()
        })
    },
    onload:function(){

    }
});