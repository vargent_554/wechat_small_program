var app = getApp();
Page({
    data:{
        currentTab:0, //预设当前项的值
        showDialog:false,
        cartNum:1,
        choiceTip:false,
        memberServer:{},
        commonUrl:app.globalData.commonUrl,
    },
    getMemberServerData:function(){
        wx.showLoading({
            title: '加载中',
        });
        app.handlerPost('/api/memberCard/detail.do',{userToken:wx.getStorageSync('sessionid')},"GET").then( (res)=>{
            if(res.data.msg=="SUCCESS"){
                let memberServer=res.data.data;
                this.setData({
                    memberServer:memberServer
                });
            }
            app.fetchAddressList();
        }).catch( (errMsg)=>{
            console.log(errMsg);
        } );
    },
    swichNav5:function(e){
        var cur=e.target.dataset.current;
        if(this.data.currentTab==cur){
            return false;
        } else{
            this.setData({
                currentTab:cur
            })
        }
    },
    choiceTip:function(){
        this.setData({
            choiceTip: !this.data.choiceTip
        });
    },
    onShareAppMessage: function (res) {
        if (res.from === 'button') {
            console.log(res.target)
        }
        return {
            title: '美璟小程序',
            path: '/pages/memberServiceDetail/memberServiceDetail?type=share&parentOpenId='+wx.getStorageSync('sessionid'),
            success: function (res) {
                console.log(res);
            },
            fail: function (res) {
                console.log(res);
            }
        }
    },
    buyMemberCart:function(e){
        wx.showLoading({
            title: '加载中',
        });
        app.handlerPost('/api/memberCard/doToBuyMemberCard.do',{userToken:wx.getStorageSync('sessionid'),id:e.currentTarget.dataset.id},"GET").then( (res)=>{
            app.fetchAddressList();
            if(res.data.code=='SUCCESS'){
                let data = res.data.data;
                wx.requestPayment({
                    'timeStamp': data.timeStamp,
                    'nonceStr': data.nonceStr,
                    'package': data.prepayId,
                    'signType': 'MD5',
                    'paySign': data.paySign,
                    'success': function (res) {
                        app.handlerPost('/api/order/wechatAppNotify.do',{userToken:wx.getStorageSync('sessionid'),orderNo:data.orderNo,payStatus:0},"GET").then( (res)=>{
                            console.log(res);
                        }).catch( (errMsg)=>{
                            console.log(errMsg);
                        } );
                        wx.showToast({
                            title: '支付成功',
                            icon: 'success',
                            duration: 3000,
                            success:function(){
                                wx.navigateTo({
                                    url: '../paySuccess/paySuccess'
                                });
                            }
                        });
                    },
                    'fail': function (res) {
                        wx.showModal({
                            title: '提示',
                            content:'付款失败'
                        });

                    },
                    'complete': function (res) {

                    }
                })
            }else{
                wx.showModal({
                    title: '提示',
                    content:res.data.msg,
                    success:function(res){
                        if(res.confirm){

                        }else if(res.cancel){

                        }
                    },
                });
            }
        }).catch( (errMsg)=>{
            console.log(errMsg);
        } );
        // wx.navigateTo({
        //     url: '../makeSureOrder/makeSureOrder'
        // })
    },
    onLoad:function(options){
        wx.login({
            success: function (r) {
                if(options.type=='share'){
                    wx.setStorageSync('parentOpenId', options.parentOpenId);
                    console.log();
                    app.login({code:r.code,parentOpenId:options.parentOpenId});
                }else{
                    app.login({code:r.code});
                }
            }
        });
        this.getMemberServerData();
    }
});