//获取应用实例
const app = getApp();
Page({
  data: {
      makeSureOrderData: [],
      addressInfo: [],
      addressData: {},
      addressId: '',
      commonUrl: app.globalData.commonUrl,
  },
    //切换地址
    changeAddressRange: function () {
        wx.setStorageSync('makeSureOrderData',this.data.makeSureOrderData);
        wx.navigateTo({
            url: '../addressManage/addressManage?choiceAddress=1&typeAddress=makeorder',
        })
    },
    payment: function () {
        var that=this;
        let options = this.data.makeSureOrderData;
        let openid = wx.getStorageSync('sessionid');
        let makeSureOrderData=JSON.stringify(that.data.makeSureOrderData);
        let addressId=this.data.addressId;
        let dataParams= {
                pdtId: options.pdtId,
                materialCode: options.materialCode,
                colourCode: options.colourCode,
                standardsCode: options.standCode,
                materialName: options.material,
                colourName: options.colour,
                standardsName: options.stand,
                num: options.cartNum,
                userToken: openid,
                addressId:this.data.addressId
        };
        if(this.data.addressData==''){
            wx.showModal({
                title: '提示',
                content:'请先添加地址',
                success:function(res){
                    if(res.confirm){

                    }else if(res.cancel){

                    }
                },
            });
            return;
        }
        app.handlerPost('/api/product/doToBuyProduct.do',dataParams,"GET").then( (res)=>{
            let data = res.data.data;
            if(res.data.code=='SUCCESS'){
                wx.requestPayment({
                    'timeStamp': data.timeStamp,
                    'nonceStr': data.nonceStr,
                    'package': data.prepayId,
                    'signType': 'MD5',
                    'paySign': data.paySign,
                    'success': function (res) {
                        wx.showToast({
                            title: '支付成功',
                            icon: 'success',
                            duration: 3000,
                            success:function(){
                                app.handlerPost('/api/order/wechatAppNotify.do',{userToken:wx.getStorageSync('sessionid'),orderNo:data.orderNo,payStatus:0},"GET").then( (res)=>{
                                    // console.log(res);
                                }).catch( (errMsg)=>{
                                    // console.log(errMsg);
                                } );
                                wx.redirectTo({
                                    url: '../paySuccess/paySuccess'
                                });
                            }
                        });

                    },
                    'fail': function (res) {
                        wx.showModal({
                            title: '提示',
                            content:'付款失败',
                            success:function(res){
                                if(res.confirm){
                                    // wx.navigateTo({
                                    //     url: '../payFail/payFail?makeSureOrderData='+makeSureOrderData+'&addressId='+addressId
                                    // });
                                    wx.redirectTo({
                                        url: '../userOrder/userOrder'
                                    });
                                }else if(res.cancel){
                                    wx.redirectTo({
                                        url: '../userOrder/userOrder'
                                    });

                                }
                            },
                        });

                    },
                    'complete': function (res) {

                    }
                })
            }else{
                wx.showModal({
                    title: '提示',
                    content:res.data.msg,
                    success:function(res){
                        if(res.confirm){

                        }else if(res.cancel){

                        }
                    },
                });
                return;
            }
        }).catch( (errMsg)=>{

        } );
    },
    getUserAddress:function(){
        wx.showLoading({
            title: '加载中',
        });
        app.handlerPost('/api/account/doMyUserAddress.do',{userToken:wx.getStorageSync('sessionid')},"GET").then( (res)=>{
            if(res.data.msg=="SUCCESS"){
                app.fetchAddressList();
                let data=res.data.data[0],addressData;
                if(data==undefined){
                    addressData=[];
                    this.setData({
                        addressData: addressData,
                    });
                }else{
                    addressData = {
                        id:data.id,
                        addressdDetail:data.area1+data.area2+data.area2+data.address,
                        buyerPhone: data.userPhone,
                        isdefault: data.defaultAddress,
                        name: data.userName,
                    };
                    this.setData({
                        addressData: addressData,
                        addressId:addressData.id
                    });
                }

            }
        }).catch( (errMsg)=>{
            console.log(errMsg);
        } );
    },
    onLoad: function (options) {
        if(wx.getStorageSync('sessionid')==''){
            wx.login({
                success: function (r) {
                    app.login({code:r.code});
                }
            });
        }
      if(options.goodDetailType=='1'){
          this.setData({
              makeSureOrderData: JSON.parse(options.makeSureOrder)
          });
          this.getUserAddress();
      }
      if(options.choiceAddressType=='1'){
          let choiceAddressData=JSON.parse(options.choiceAddressData);
            let addressData ={
                id:choiceAddressData.id,
                addressdDetail:choiceAddressData.addressdDetail+choiceAddressData.address,
                buyerPhone: choiceAddressData.buyerPhone,
                isdefault: choiceAddressData.isdefault,
                name:choiceAddressData.name,
            };
            this.setData({
                addressData: addressData,
                addressId:addressData.id,
                makeSureOrderData:wx.getStorageSync('makeSureOrderData')
            });
        }

    },
    onPullDownRefresh: function () {
        // 小程序提供的api，通知页面停止下拉刷新效果
        setTimeout(function () {
            wx.stopPullDownRefresh();
        }, 1000)
    },
});
