//获取应用实例
const app = getApp();
Page({
    data: {
       commissionDetailData:{}
    },
    //跳到体现明细
    toPutForWordDetail: function() {
      wx.navigateTo({
        url: '../putForwordDetail/putForwordDetail?type=commisionFor'
      })
    },
    getPersonCommissionDetailData:function(){
        let accountId=wx.getStorageSync('accountId');
        app.handlerPost('/api/account/doMyAccountInfo.do',{userToken:wx.getStorageSync('sessionid')}).then( (res)=>{
            if(res.data.code=='SUCCESS'){
                let data=res.data.data;
                this.setData({
                    commissionDetailData:data
                });
            }
        }).catch( (errMsg)=>{
            console.log(errMsg);
        } );

    },
    getChildCommissionDetailData:function(){
        let accountId=wx.getStorageSync('accountId');
        app.handlerPost('/api/account/doMyAccountInfo.do',{userToken:wx.getStorageSync('sessionid')}).then( (res)=>{
            if(res.data.code=='SUCCESS'){
                let data=res.data.data;
                this.setData({
                    commissionDetailData:data
                });
            }
        }).catch( (errMsg)=>{
            console.log(errMsg);
        } );

    },
    onLoad:function(options){
        if(wx.getStorageSync('sessionid')==''){
            wx.login({
                success: function (r) {
                    app.login({code:r.code});
                }
            });
        }
        if(options.type=="person"){
            this.getPersonCommissionDetailData();
        }

    }
});