var app = getApp();
Page({
    data:{
        companyData:'请输入您的公司类型',
        setCompanyDataC:false,
        showDialog:false,
        companyChoiceData:[
            "设计公司","物业公司","装修公司","房产公司","建材公司","设计媒体"
        ],
        isShare:false,
        parentOpenId:'',
        setCompanyData:false,
    },
    bindChange: function(e) {
        this.setData({
            setCompanyData:true,
        });
        const choiceComD = e.detail.value;
        this.data.companyData= this.data.companyChoiceData[choiceComD];
    },
    choiceCompanyData: function (e) {
        this.setData({
            showDialog:!this.data.showDialog
        });
    },
    choiceCompanySure: function (e) {
        if(this.data.setCompanyData){
            this.setData({
                showDialog:!this.data.showDialog,
                companyData:  this.data.companyData
            });
        }else{
            this.setData({
                showDialog:!this.data.showDialog,
                companyData:  this.data.companyChoiceData[0]
            });
        }

        if( this.data.companyData!='请输入您的公司类型'){
            this.setData({
                setCompanyDataC:true
            });
        }
    },
    //手机注册
    doRegisterInfo:function(data){
        wx.showLoading({
            title: '加载中',
        });
        let  userinfo=wx.getStorageSync('myUserIInfo');
        let rigisterParams={
            userToken:wx.getStorageSync('sessionid'),
            nickName:userinfo.userName,
            headimgurl:userinfo.headImg,
            openid:wx.getStorageSync('sessionid'),
            typeName:data.companyType, //公司类型
            corpName:data.companyName, //公司名称
            legalPerson:data.nameValue,//法人代表
            contactTel:data.phoneValue, //手机号
            contactName:data.nameValue, //联系人
            address:data.emailValue, //邮箱地址
            passWord:data.passValue, //密码
        };
        if(wx.getStorageSync('parentOpenId')&&wx.getStorageSync('parentOpenId')!=wx.getStorageSync('sessionid')){
            rigisterParams.parentOpenId=this.data.parentOpenId;
            rigisterParams.openid=wx.getStorageSync('sessionid');
            rigisterParams.userToken=wx.getStorageSync('sessionid');
            app.handlerPost('/api/account/doRegisterSubAccount.do',rigisterParams,"GET").then( (res)=>{
                if(res.data.msg=="SUCCESS"){
                    wx.showToast({
                        title: '注册成功',
                        icon: 'success',
                        duration: 2000
                    });
                    wx.switchTab({
                        url:'../index/index',
                        success:function(){
                        },
                    })
                }
            }).catch( (errMsg)=>{
                console.log(errMsg);
            } );
        }else{
            app.handlerPost('/api/account/doRegisterAgentUser.do',rigisterParams,"GET").then( (res)=>{
                if(res.data.msg=="SUCCESS"){
                    wx.showToast({
                        title: '注册成功  等待审核',
                        icon: 'success',
                        duration: 2000
                    });
                    wx.switchTab({
                        url:'../index/index',
                        success:function(){
                        },
                    })

                }
            }).catch( (errMsg)=>{
                console.log(errMsg);
            } );
        }
        app.fetchAddressList();

    },
    formSubmit: function(e) {
        let warn = "",
            phonetest=/^1(3|4|5|7|8)\d{9}$/,
            emailtest=/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        var flag = true;
        if(e.detail.value.companyType=="请输入您的公司类型"){
            warn = "请输入您的公司类型！";
        }else if(e.detail.value.companyName==""){
            warn = "请选择您公司名称！";
        }else if(e.detail.value.nameValue==''){
            warn = "请填写您的真实姓名"
        }else if(e.detail.value.phoneValue==""){
            warn = "请填写您的手机号！";
        }else if(!phonetest.test(e.detail.value.phoneValue)){
            warn = "手机号格式不正确";
        }else if(e.detail.value.emailValue==''){
            warn = "请输入您的电子邮箱";
        }else if(!emailtest.test(e.detail.value.emailValue)){
            warn = "请输入正确的电子邮箱";
        }else if(e.detail.value.passValue==''){
            warn = "请输入您的密码";
        }else{
            flag=false;
            this.doRegisterInfo(e.detail.value);

        };
        if(flag==true) {
            wx.showModal({
                icon:'error',
                content: warn
            })
        }
    },
    onShareAppMessage: function (res) {
        if (res.from === 'button') {
            console.log(res.target)
        }
        return {
            title: '美璟小程序',
            path: '/pages/userRegister/userRegister?parentOpenId='+wx.getStorageSync('sessionid')+'&type=share',
            success: function (res) {
                console.log(res);
            },
            fail: function (res) {
                console.log(res);
            }
        }
    },
    onLoad:function(options){
        if(wx.getStorageSync('sessionid')==''){
            wx.login({
                success: function (r) {
                    app.login({code:r.code});
                }
            });
        }
        if(options.parentOpenId==undefined||options.parentOpenId==''){
            this.setData({
                isShare:false
            });
        }else{
            this.setData({
                isShare:true,
                parentOpenId:options.parentOpenId
            });
            wx.setStorageSync('parentOpenId', options.parentOpenId);
        }
    }
});