//app.js
App({
    onLaunch: function () {
      // 展示本地存储能力
      var logs = wx.getStorageSync('logs') || [];
      logs.unshift(Date.now());
      wx.setStorageSync('logs', logs);
        // wx.reLaunch({
        //     url:'pages/flickerScreen/flickerScreen'
        // })
        //调用微信登录接口
        
    },
      globalData: {
          commonUrl:'https://mzgf.oss-cn-hangzhou.aliyuncs.com/',
          userInfo: null,
          setValue:false,
          openid:wx.getStorageSync('sessionid'),
          showDialogQuanlity:false,
      },
     handlerPost:function(url,data,type){
          var promise = new Promise((resolve, reject) => {
              wx.request({
                  url:'https://api.mzgf.com/'+ url,
                  data: data,
                  method: type,
                  header: { 'content-type': 'application/x-www-form-urlencoded' },
                  success: function (res) {
                        resolve(res);
                  },
                  error: function (e) {
                      reject(e);
                  }
              })
          });
          return promise;
      },
      fetchAddressList() {
          setTimeout(res => {
              wx.hideLoading();
          }, 500);
  
      },
      login: function (param) {
        var that=this;
          wx.request({
            url: 'https://api.mzgf.com/api/account/doGetOpenId.do',
              data: param,
              header: {
                  'content-type': "application/json",
              },
              success: function (res) {
                  if (res) {
                      if(res.data.msg=='SUCCESS'){
                          console.log(res.data.data);
                          console.log('已获得过用户授权信息');
                          that.handlerPost('/api/account/doRegisterUser.do', {
                              code: param.code,
                              parentOpenId: param.parentOpenId,
                              nickname: res.data.data.nickName,
                              headimgurl:res.data.data.headimgurl
                          }).then( (res)=>{
                              console.log(res)
                          });
                          if(res.data.data.openid){
                              wx.setStorageSync('sessionid', res.data.data.openid);
                              wx.setStorageSync('nickname', res.data.data.nickname);
                          }
                       }else{
                          console.log('没有得过用户授权信息,重新授权');
                          wx.redirectTo({
                              url:'../login/login'
                          });
                      }
                  }
  
              }
          })
      },
      onLoad:function(){
          wx.login({
              success: function (r) {
                 this.login({code:r.code});
              }
          });
      },
      getCommonTime:function(){
          Date.prototype.Format = function (fmt) { // author: meizz
              var o = {
                  "M+": this.getMonth() + 1, // 月份
                  "d+": this.getDate(), // 日
                  "h+": this.getHours(), // 小时
                  "m+": this.getMinutes(), // 分
                  "s+": this.getSeconds(), // 秒
                  "q+": Math.floor((this.getMonth() + 3) / 3), // 季度
                  "S": this.getMilliseconds() // 毫秒
              };
              if (/(y+)/.test(fmt))
                  fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
              for (var k in o)
                  if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
              return fmt;
          }
      },
      getTrim:function(){
        String.prototype.trim = function() {
              return  this.replace(/^\s+|\s+$/g, '');
          }
      },
    
  });